/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import "ImageToolBase.h"

@implementation ImageToolBase

- (id)initWithImageEditor:(_CLImageEditorViewController*)editor withToolInfo:(ImageToolInfo*)info
{
    self = [super init];
    if(self){
        self.editor   = editor;
        self.toolInfo = info;
    }
    return self;
}

+ (NSString*)defaultIconImagePath
{
    return [NSString stringWithFormat:@"%@", NSStringFromClass([self class])];
    //return [NSString stringWithFormat:@"%@.bundle/%@/icon.png", [ImageEditorTheme bundleName], NSStringFromClass([self class])];
}

+ (CGFloat)defaultDockedNumber
{
    
    // Image tools are sorted in the Toolbar ScrollView accordingly to the following Array:
    NSArray *tools = @[
                       @"TextTool",
                       @"FilterTool",
                       @"StickerTool",
                       @"FramesTool",
                       @"BrightnessTool",
                       @"BlurTool",
                       @"ClippingTool",
                       
                       ];
    return [tools indexOfObject:NSStringFromClass(self)];
}

+ (NSArray*)subtools
{
    return nil;
}

+ (NSString*)defaultTitle
{
    return @"DefaultTitle";
}

+ (BOOL)isAvailable
{
    return NO;
}

+ (NSDictionary*)optionalInfo
{
    return nil;
}

#pragma mark - SETUP ====================

- (void)setup
{


}

- (void)cleanup
{
    
}

- (void)executeWithCompletionBlock:(void(^)(UIImage *image, NSError *error, NSDictionary *userInfo))completionBlock
{
    completionBlock(self.editor.imageView.image, nil, nil);
}

-(void)refreshViewAfterInApp
{
    
    
}


@end
