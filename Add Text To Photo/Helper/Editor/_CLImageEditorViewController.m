//
//  _CLImageEditorViewController.m
//
//  Created by sho yakushiji on 2013/11/05.
//  Copyright (c) 2013年 CALACULU. All rights reserved.
//

#import "_CLImageEditorViewController.h"

#import "ImageToolBase.h"
#import "Configs.h"
#import <UIKit/UIKit.h>
#import "ImageToolSettings.h"
#import "WaterMarkCreator.h"

#import <StoreKit/StoreKit.h>

#pragma mark- _CLImageEditorViewController

static const CGFloat kNavBarHeight = 44.0f;
static const CGFloat kMenuBarHeight = 95.0f;

@interface _CLImageEditorViewController()
<ImageToolProtocol>

@property (nonatomic, strong) ImageToolBase *currentTool;
@property (nonatomic, strong, readwrite) ImageToolInfo *toolInfo;
@property (nonatomic, strong) UIImageView *targetImageView;


@end


@implementation _CLImageEditorViewController
{
    UIImage *_originalImage;
    UIView *_bgView;
    UIImage *newImage;
    
    UIImageView* waterMark;
}
@synthesize toolInfo = _toolInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
       // self.toolInfo = [CLImageToolInfo toolInfoForToolClass:[self class]];
    }
    return self;
}

- (id)init
{
    self = [self initWithNibName:@"ImageEditorViewController" bundle:nil];
    if (self){
        _toolInfo = [ImageToolInfo toolInfoForToolClass:[self class]];
    }
    return self;
}

- (id)initWithImage:(UIImage *)image
{
    return [self initWithImage:image delegate:nil];
}

- (id)initWithImage:(UIImage*)image delegate:(id<ImageEditorDelegate>)delegate
{
    self = [self init];
    if (self){
        _originalImage = [image deepCopy];
        self.delegate = delegate;
    }
    return self;
}

- (id)initWithDelegate:(id<ImageEditorDelegate>)delegate
{
    self = [self init];
    if (self){
        self.delegate = delegate;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSLog(@"viewWillAppear in imageeditor");
    
    
    // Set the Title's font and color
    _navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                            [UIColor whiteColor],
                                            NSForegroundColorAttributeName, NAVBAR_FONT,
                                            NSFontAttributeName,nil];
    
}

//- (void)dealloc
//{
//    [_navigationBar removeFromSuperview];
//}

#pragma mark- Custom initialization

//- (UIBarButtonItem*)createDoneButton
//{
//    UIBarButtonItem *rightBarButtonItem = nil;
//    NSString *doneBtnTitle = [CLImageEditorTheme localizedString:@"CLImageEditor_DoneBtnTitle" withDefault:nil];
//
//    if(![doneBtnTitle isEqualToString:@"CLImageEditor_DoneBtnTitle"]){
//
//        rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:doneBtnTitle style:UIBarButtonItemStyleDone target:self action:@selector(pushedFinishBtn:)];
//    }
//    else{
//        rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pushedFinishBtn:)];
//    }
//
//
//    return rightBarButtonItem;
//}
//
//- (void)initNavigationBar
//{
//    self.navigationItem.rightBarButtonItem = [self createDoneButton];
//    [self.navigationController setNavigationBarHidden:NO animated:NO];
//
//    if(_navigationBar==nil){
//        UINavigationItem *navigationItem  = [[UINavigationItem alloc] init];
//        navigationItem.leftBarButtonItem  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(pushedCloseBtn:)];
//        navigationItem.rightBarButtonItem = [self createDoneButton];
//
//        CGFloat dy = MIN([UIApplication sharedApplication].statusBarFrame.size.height, [UIApplication sharedApplication].statusBarFrame.size.width);
//
//        UINavigationBar *navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, dy, self.view.width, kNavBarHeight)];
//        navigationBar.barTintColor = [UIColor blackColor];
//        [navigationBar pushNavigationItem:navigationItem animated:NO];
//        navigationBar.delegate = self;
//
//        if(self.navigationController){
//            [self.navigationController.view addSubview:navigationBar];
//            [_CLImageEditorViewController setConstraintsLeading:@0 trailing:@0 top:nil bottom:nil height:@(kNavBarHeight) width:nil parent:self.navigationController.view child:navigationBar peer:nil];
//        }
//        else{
//            [self.view addSubview:navigationBar];
//            if (@available(iOS 11.0, *)) {
//                [_CLImageEditorViewController setConstraintsLeading:@0 trailing:@0 top:nil bottom:nil height:@(kNavBarHeight) width:nil parent:self.view child:navigationBar peer:nil];
//                [_CLImageEditorViewController setConstraintsLeading:nil trailing:nil top:@0 bottom:nil height:nil width:nil parent:self.view child:navigationBar peer:self.view.safeAreaLayoutGuide];
//            } else {
//                [_CLImageEditorViewController setConstraintsLeading:@0 trailing:@0 top:@(dy) bottom:nil height:@(kNavBarHeight) width:nil parent:self.view child:navigationBar peer:nil];
//            }
//        }
//        _navigationBar = navigationBar;
//    }
//
//    if(self.navigationController!=nil){
//        _navigationBar.frame  = self.navigationController.navigationBar.frame;
//        _navigationBar.hidden = YES;
//        [_navigationBar popNavigationItemAnimated:NO];
//    }
//    else{
//        _navigationBar.topItem.title = self.title;
//
//    }
//
//    if([UIDevice iosVersion] < 7){
//        _navigationBar.barStyle = UIBarStyleBlackTranslucent;
//    }
//}
//
//- (void)initMenuScrollView
//{
//    if(self.menuView==nil){
//        UIScrollView *menuScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, kMenuBarHeight)];
//
//        // Adjust for iPhone X
//        if (@available(iOS 11.0, *)) {
//            UIEdgeInsets theInsets = [UIApplication sharedApplication].keyWindow.rootViewController.view.safeAreaInsets;
//            menuScroll.height += theInsets.bottom;
//        }
//
//        menuScroll.top = self.view.height - menuScroll.height;
//        menuScroll.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
//        menuScroll.showsHorizontalScrollIndicator = NO;
//        menuScroll.showsVerticalScrollIndicator = NO;
//
//        [self.view addSubview:menuScroll];
//        self.menuView = menuScroll;
//        [_CLImageEditorViewController setConstraintsLeading:@0 trailing:@0 top:nil bottom:@0 height:@(menuScroll.height) width:nil parent:self.view child:menuScroll peer:nil];
//    }
//    self.menuView.backgroundColor = [CLImageEditorTheme toolbarColor];
//}
//
//- (void)initImageScrollView
//{
//    if(_scrollView==nil){
//        UIScrollView *imageScroll = [[UIScrollView alloc] initWithFrame:self.view.bounds];
//        imageScroll.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//        imageScroll.showsHorizontalScrollIndicator = NO;
//        imageScroll.showsVerticalScrollIndicator = NO;
//        imageScroll.delegate = self;
//        imageScroll.clipsToBounds = NO;
//
//        CGFloat y = 0;
//        if(self.navigationController){
//            if(self.navigationController.navigationBar.translucent){
//                y = self.navigationController.navigationBar.bottom;
//            }
//            y = ([UIDevice iosVersion] < 7) ? y-[UIApplication sharedApplication].statusBarFrame.size.height : y;
//        }
//        else{
//            y = _navigationBar.bottom;
//        }
//
//        imageScroll.top = y;
//        imageScroll.height = self.view.height - imageScroll.top - _menuView.height;
//
//        [self.view insertSubview:imageScroll atIndex:0];
//        _scrollView = imageScroll;
//
//        if (@available(iOS 11.0, *)) {
//            [_CLImageEditorViewController setConstraintsLeading:@0 trailing:@0 top:nil bottom:@(-_menuView.height) height:nil width:nil parent:self.view child:imageScroll peer:nil];
//            [_CLImageEditorViewController setConstraintsLeading:nil trailing:nil top:@(y) bottom:nil height:nil width:nil parent:self.view child:imageScroll peer:self.view.safeAreaLayoutGuide];
//        }
//        else{
//            [_CLImageEditorViewController setConstraintsLeading:@0 trailing:@0 top:@(y) bottom:@(-_menuView.height) height:nil width:nil parent:self.view child:imageScroll peer:nil];
//        }
//
//    }
//}
//
//+(NSArray <NSLayoutConstraint *>*)setConstraintsLeading:(NSNumber *)leading
//                                               trailing:(NSNumber *)trailing
//                                                    top:(NSNumber *)top
//                                                 bottom:(NSNumber *)bottom
//                                                 height:(NSNumber *)height
//                                                  width:(NSNumber *)width
//                                                 parent:(UIView *)parent
//                                                  child:(UIView *)child
//                                                   peer:(nullable id)peer
//{
//    NSMutableArray <NSLayoutConstraint *>*constraints = [NSMutableArray new];
//    //Trailing
//    if (trailing) {
//        NSLayoutConstraint *trailingConstraint = [NSLayoutConstraint
//                                                  constraintWithItem:child
//                                                  attribute:NSLayoutAttributeTrailing
//                                                  relatedBy:NSLayoutRelationEqual
//                                                  toItem:(peer ?: parent)
//                                                  attribute:NSLayoutAttributeTrailing
//                                                  multiplier:1.0f
//                                                  constant:trailing.floatValue];
//        [parent addConstraint:trailingConstraint];
//        [constraints addObject:trailingConstraint];
//    }
//    //Leading
//    if (leading) {
//        NSLayoutConstraint *leadingConstraint = [NSLayoutConstraint
//                                                 constraintWithItem:child
//                                                 attribute:NSLayoutAttributeLeading
//                                                 relatedBy:NSLayoutRelationEqual
//                                                 toItem:(peer ?: parent)
//                                                 attribute:NSLayoutAttributeLeading
//                                                 multiplier:1.0f
//                                                 constant:leading.floatValue];
//        [parent addConstraint:leadingConstraint];
//        [constraints addObject:leadingConstraint];
//    }
//    //Bottom
//    if (bottom) {
//        NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint
//                                                constraintWithItem:child
//                                                attribute:NSLayoutAttributeBottom
//                                                relatedBy:NSLayoutRelationEqual
//                                                toItem:(peer ?: parent)
//                                                attribute:NSLayoutAttributeBottom
//                                                multiplier:1.0f
//                                                constant:bottom.floatValue];
//        [parent addConstraint:bottomConstraint];
//        [constraints addObject:bottomConstraint];
//    }
//    //Top
//    if (top) {
//        NSLayoutConstraint *topConstraint = [NSLayoutConstraint
//                                             constraintWithItem:child
//                                             attribute:NSLayoutAttributeTop
//                                             relatedBy:NSLayoutRelationEqual
//                                             toItem:(peer ?: parent)
//                                             attribute:NSLayoutAttributeTop
//                                             multiplier:1.0f
//                                             constant:top.floatValue];
//        [parent addConstraint:topConstraint];
//        [constraints addObject:topConstraint];
//    }
//    //Height
//    if (height) {
//        NSLayoutConstraint *heightConstraint = [NSLayoutConstraint
//                                                constraintWithItem:child
//                                                attribute:NSLayoutAttributeHeight
//                                                relatedBy:NSLayoutRelationEqual
//                                                toItem:nil
//                                                attribute:NSLayoutAttributeNotAnAttribute
//                                                multiplier:1.0f
//                                                constant:height.floatValue];
//        [child addConstraint:heightConstraint];
//        [constraints addObject:heightConstraint];
//    }
//    //Width
//    if (width) {
//        NSLayoutConstraint *widthConstraint = [NSLayoutConstraint
//                                               constraintWithItem:child
//                                               attribute:NSLayoutAttributeWidth
//                                               relatedBy:NSLayoutRelationEqual
//                                               toItem:nil
//                                               attribute:NSLayoutAttributeNotAnAttribute
//                                               multiplier:1.0f
//                                               constant:width.floatValue];
//        [child addConstraint:widthConstraint];
//        [constraints addObject:widthConstraint];
//    }
//    child.translatesAutoresizingMaskIntoConstraints = NO;
//    return constraints;
//}


-(void)inAppScreenClosed
{
    
   // [self.currentTool refreshViewAfterInApp];
    
}



#pragma mark-

//- (void)showInViewController:(UIViewController*)controller withImageView:(UIImageView*)imageView;
//{
//    _originalImage = imageView.image;
//
//    self.targetImageView = imageView;
//
//    [controller addChildViewController:self];
//    [self didMoveToParentViewController:controller];
//
//    self.view.frame = controller.view.bounds;
//    [controller.view addSubview:self.view];
//    [self refreshImageView];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.toolInfo.title;
    
    //add watermark image
    
    //[self resetImageViewFrame];
    
//    self.view.clipsToBounds = YES;
//    self.view.backgroundColor = self.theme.backgroundColor;
//    self.navigationController.view.backgroundColor = self.view.backgroundColor;
    
    if([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]){
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    if([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]){
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    if (self.navigationController != nil){
        [self.navigationController setNavigationBarHidden:false animated:true];
        _navigationBar.hidden = true;
        [_navigationBar popNavigationItemAnimated:false];
    } else {
        _navigationBar.topItem.title = self.title;
    }
    
    //[self refreshToolSettings];
    
    [self setMenuView];
    
    if(_imageView==nil){
        
        _imageView = [UIImageView new];
        self.scrollView.imageView = self.imageView;
//        self.scrollView.minimumZoomScale = 1.0;
//        self.scrollView.maximumZoomScale = 1.0;
//        self.scrollView.delegate = self;
        
        self.scrollView.imageView.backgroundColor = [UIColor redColor];

      
//        [self.scrollView addSubview:_imageView];

     

    }
    

   // [self resetZoomScaleWithAnimated:true];
    [self resetImageViewFrame];
    
    
    if (_targetImageView){
        [self expropriateImageView];
        
    } else {
        [self refreshImageView];
    }
    
  
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    
    float bottom = CGRectGetMaxY(self.scrollView.imageView.frame);
    
    NSLog(@"bottom......%f",bottom);
    
    waterMark = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"waterMark.png"]];
    //waterMark.contentMode = UIViewContentModeScaleAspectFit;
    
    [waterMark setFrame:CGRectMake(_imageView.frame.size.width - 50, bottom + 10, 30, 30)];
    
    float buttonbottom = CGRectGetMaxY(waterMark.frame);
    
    NSLog(@"buttonbottom......%f",buttonbottom);
    [self.view addSubview:waterMark];
    
    [self changeFrame];
}

- (void)changeFrame{
    
    [UIView animateWithDuration:3.0 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^(void) {
                         //do your half rotation here
                         self->waterMark.transform = CGAffineTransformMakeScale(1, 1);
                     }
                     completion:^(BOOL finished) {
                         if(finished){
                             
                         //    NSLog(@"finished");
//                             self->waterMark.image = [UIImage imageNamed:@"logo_with_name.png"];
                             [UIView animateWithDuration:3.0 delay:0.0 options:UIViewAnimationOptionCurveEaseOut
                                              animations:^(void) {
                                                  //do your second half rotation here
                                                  self->waterMark.transform = CGAffineTransformMakeScale(-1, 1);
                                              } completion:^(BOOL finished) {
                                                  
                                              //    NSLog(@"finished2");
                                                  [self changeFrame];
                                                  
                                              }];
                         }
                     }];
}

//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//
//    if(self.targetImageView){
//        [self expropriateImageView];
//    }
//    else{
//        [self refreshImageView];
//    }
//}

#pragma mark- View transition

- (void)copyImageViewInfo:(UIImageView*)fromView toView:(UIImageView*)toView
{
    CGAffineTransform transform = fromView.transform;
    fromView.transform = CGAffineTransformIdentity;
    
    toView.transform = CGAffineTransformIdentity;
    toView.frame = [toView.superview convertRect:fromView.frame fromView:fromView.superview];
    toView.transform = transform;
    toView.image = fromView.image;
    toView.contentMode = fromView.contentMode;
    toView.clipsToBounds = fromView.clipsToBounds;
    
    fromView.transform = transform;
}

- (void)expropriateImageView
{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    
    UIImageView *animateView = [UIImageView new];
    [window addSubview:animateView];
    [self copyImageViewInfo:self.targetImageView toView:animateView];
    
    _bgView = [[UIView alloc] initWithFrame:self.view.bounds];
    [self.view insertSubview:_bgView atIndex:0];
    
    _bgView.backgroundColor = self.view.backgroundColor;
    self.view.backgroundColor = [self.view.backgroundColor colorWithAlphaComponent:0];
    
    self.targetImageView.hidden = YES;
    _imageView.hidden = YES;
    _bgView.alpha = 0;
    _navigationBar.transform = CGAffineTransformMakeTranslation(0, -_navigationBar.height);
    _menuView.transform = CGAffineTransformMakeTranslation(0, self.view.height-_menuView.top);
    
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         animateView.transform = CGAffineTransformIdentity;
                         
                         CGFloat dy = ([UIDevice iosVersion]<7) ? [UIApplication sharedApplication].statusBarFrame.size.height : 0;
                         
                         CGSize size = (self->_imageView.image) ? self->_imageView.image.size : self->_imageView.frame.size;
                         if(size.width>0 && size.height>0){
                             CGFloat ratio = MIN(self.scrollView.width / size.width, self.scrollView.height / size.height);
                             CGFloat W = ratio * size.width;
                             CGFloat H = ratio * size.height;
                             animateView.frame = CGRectMake((self.scrollView.width-W)/2 + self.scrollView.left, (self.scrollView.height-H)/2 + self.scrollView.top + dy, W, H);
                         }
                         
                         self->_bgView.alpha = 1;
                         self->_navigationBar.transform = CGAffineTransformIdentity;
                         self->_menuView.transform = CGAffineTransformIdentity;
                     }
                     completion:^(BOOL finished) {
                         self.targetImageView.hidden = NO;
                         self->_imageView.hidden = NO;
                         [animateView removeFromSuperview];
                     }
     ];
}

- (void)restoreImageView:(BOOL)canceled
{
    if(!canceled){
        self.targetImageView.image = _imageView.image;
    }
    self.targetImageView.hidden = YES;
    
    id<ImageEditorTransitionDelegate> delegate = [self transitionDelegate];
    if([delegate respondsToSelector:@selector(imageEditor:willDismissWithImageView:canceled:)]){
        [delegate imageEditor:self willDismissWithImageView:self.targetImageView canceled:canceled];
    }
    
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    
    UIImageView *animateView = [UIImageView new];
    [window addSubview:animateView];
    [self copyImageViewInfo:_imageView toView:animateView];
    
    _menuView.frame = [window convertRect:_menuView.frame fromView:_menuView.superview];
    _navigationBar.frame = [window convertRect:_navigationBar.frame fromView:_navigationBar.superview];
    
    [window addSubview:_menuView];
    [window addSubview:_navigationBar];
    
    self.view.userInteractionEnabled = NO;
    _menuView.userInteractionEnabled = NO;
    _navigationBar.userInteractionEnabled = NO;
    _imageView.hidden = YES;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self->_bgView.alpha = 0;
                         self->_menuView.alpha = 0;
                         self->_navigationBar.alpha = 0;
                         
                         self->_menuView.transform = CGAffineTransformMakeTranslation(0, self.view.height-self->_menuView.top);
                         self->_navigationBar.transform = CGAffineTransformMakeTranslation(0, -self->_navigationBar.height);
                         
                         [self copyImageViewInfo:self.targetImageView toView:animateView];
                     }
                     completion:^(BOOL finished) {
                         [animateView removeFromSuperview];
                         [self->_menuView removeFromSuperview];
                         [self->_navigationBar removeFromSuperview];
                         
                         [self willMoveToParentViewController:nil];
                         [self.view removeFromSuperview];
                         [self removeFromParentViewController];
                         
                         self->_imageView.hidden = NO;
                         self.targetImageView.hidden = NO;
                         
                         if([delegate respondsToSelector:@selector(imageEditor:didDismissWithImageView:canceled:)]){
                             [delegate imageEditor:self didDismissWithImageView:self.targetImageView canceled:canceled];
                         }
                     }
     ];
}

#pragma mark- Properties

- (id<ImageEditorTransitionDelegate>)transitionDelegate
{
    if([self.delegate conformsToProtocol:@protocol(ImageEditorTransitionDelegate)]){
        return (id<ImageEditorTransitionDelegate>)self.delegate;
    }
    return nil;
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    self.toolInfo.title = title;
}

//- (UIScrollView*)scrollView
//{
//    return _scrollView;
//}

#pragma mark- ImageTool setting

+ (NSString*)defaultIconImagePath
{
    return nil;
}

+ (CGFloat)defaultDockedNumber
{
    return 0;
}

+ (NSString*)defaultTitle
{
//    return [ImageEditorTheme localizedString:@"ImageEditor_DefaultTitle" withDefault:@"Edit"];
    
    return NSLocalizedString(@"EDIT", @"");
    

}

+ (BOOL)isAvailable
{
    return YES;
}

+ (NSArray*)subtools
{
    return [ImageToolInfo toolsWithToolClass:[ImageToolBase class]];
}

+ (NSDictionary*)optionalInfo
{
    return nil;
}

#pragma mark - SET MENU VIEW ================================


- (void)setMenuView {
    CGFloat x = 0;
    CGFloat W = 70;
    CGFloat H = _menuView.height;
    
    for(ImageToolInfo *info in self.toolInfo.sortedSubtools){
        if(!info.available){
            continue;
        }
        
        ToolbarMenuItem *view = [ImageEditorTheme menuItemWithFrame:CGRectMake(x, 0, W, H) target:self action:@selector(tappedMenuView:) toolInfo:info];
        
        [_menuView addSubview:view];
        x += W;
    }
    _menuView.contentSize = CGSizeMake(MAX(x, _menuView.frame.size.width+1), 0);
}

- (void)resetImageViewFrame  {
    
//    NSLog(@"resetImageViewFrame");
//
//    CGSize size = (_imageView.image) ? _imageView.image.size : _imageView.frame.size;
//      NSLog(@"size: %f - %f", size.width, size.height);
//
//
//    if(size.width > 0   &&   size.height > 0) {
//        CGFloat ratio = MIN(self.scrollView.frame.size.width / size.width, self.scrollView.frame.size.height / size.height);
//        CGFloat W = ratio * size.width * self.scrollView.zoomScale;
//        CGFloat H = ratio * size.height * self.scrollView.zoomScale - 20;
//        _imageView.frame = CGRectMake((self.scrollView.width-W)/2, (self.scrollView.height-H)/2-5, W, H);
//    }
//
//    NSLog(@"self->_imageView......???????? %@",NSStringFromCGRect(self->_imageView.frame));
//
//
//    self.scrollView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin);
    
    CGSize size = (_imageView.image) ? _imageView.image.size : _imageView.frame.size;
    if(size.width>0 && size.height>0){
        CGFloat ratio = MIN(_scrollView.frame.size.width / size.width, _scrollView.frame.size.height / size.height);
        CGFloat W = ratio * size.width * _scrollView.zoomScale;
        CGFloat H = ratio * size.height * _scrollView.zoomScale;
        
        _imageView.frame = CGRectMake(MAX(0, (_scrollView.width-W)/2), MAX(0, (_scrollView.height-H)/2), W, H);
    }
}


- (void)fixZoomScaleWithAnimated:(BOOL)animated {
    
    NSLog(@"fixZoomScaleWithAnimated %f",self.scrollView.zoomScale);
    
    CGFloat minZoomScale = self.scrollView.minimumZoomScale;
    self.scrollView.maximumZoomScale = 0.95*minZoomScale;
    self.scrollView.minimumZoomScale = 0.95*minZoomScale;
    [self.scrollView setZoomScale:self.scrollView.minimumZoomScale animated:animated];
}

//- (void)resetZoomScaleWithAnimated:(BOOL)animated {
//    //  NSLog(@"resetZoomScaleWithAnimated %f",_scrollView.zoomScale);
//    //self.scrollView.zoomScale = 2;
//    self.scrollView.maximumZoomScale = 1.0;
//    self.scrollView.minimumZoomScale = 3.0;
//   // self.scrollView.contentSize=CGSizeMake(self.imageView.frame.size.width,       self.imageView.frame.size.height);
////    self.scrollView.clipsToBounds=YES;
//    [self.scrollView setZoomScale:self.scrollView.minimumZoomScale animated:animated];
//
//    NSLog(@"resetZoomScaleWithAnimated %f",self.scrollView.zoomScale);
//
//}

- (void)refreshImageView
{
    _imageView.image = _originalImage;
    
    [self resetImageViewFrame];
}



- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}

- (BOOL)shouldAutorotate
{
    return (_currentTool == nil);
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self resetImageViewFrame];
   // [self refreshToolSettings];
    [self scrollViewDidZoom:_scrollView];
}
//
//- (BOOL)prefersStatusBarHidden
//{
//    return [[CLImageEditorTheme theme] statusBarHidden];
//}
//
//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return [[CLImageEditorTheme theme] statusBarStyle];
//}

#pragma mark- Tool actions

- (void)setCurrentTool:(ImageToolBase *)currentTool
{
    if(currentTool != _currentTool){
        [_currentTool cleanup];
        _currentTool = currentTool;
        [_currentTool setup];
        
        [self swapToolBarWithEditing:(_currentTool!=nil)];
    }
}

#pragma mark- Menu actions

- (void)swapMenuViewWithEditing:(BOOL)editing
{
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         if(editing){
                             self->_menuView.transform = CGAffineTransformMakeTranslation(0, self.view.height-self->_menuView.top);
                         }
                         else{
                             self->_menuView.transform = CGAffineTransformIdentity;
                         }
                     }
     ];
}

- (void)swapNavigationBarWithEditing:(BOOL)editing
{
    
    self.cancleButton.hidden = NO;
    self.doneButton.hidden = NO;
    
    self.closeButton.hidden = YES;
    self.saveButton.hidden = YES;
    
    if(self.navigationController==nil){
        return;
    }
    
    
    if(editing){
        _navigationBar.hidden = NO;
        _navigationBar.transform = CGAffineTransformMakeTranslation(0, -_navigationBar.height);
        
        [UIView animateWithDuration:kImageToolAnimationDuration
                         animations:^{
                             self.navigationController.navigationBar.transform = CGAffineTransformMakeTranslation(0, -self.navigationController.navigationBar.height-20);
                             self->_navigationBar.transform = CGAffineTransformIdentity;
                         }
         ];
    }
    else{
        [UIView animateWithDuration:kImageToolAnimationDuration
                         animations:^{
                             self.navigationController.navigationBar.transform = CGAffineTransformIdentity;
                             self->_navigationBar.transform = CGAffineTransformMakeTranslation(0, -self->_navigationBar.height);
                         }
                         completion:^(BOOL finished) {
                             self->_navigationBar.hidden = YES;
                             self->_navigationBar.transform = CGAffineTransformIdentity;
                         }
         ];
    }
}

- (void)swapToolBarWithEditing:(BOOL)editing
{
    [self swapMenuViewWithEditing:editing];
    [self swapNavigationBarWithEditing:editing];
    
    self.navTitleLabel.text = self.currentTool.toolInfo.title;
    
}

- (void)setupToolWithToolInfo:(ImageToolInfo*)info
{
    if(self.currentTool){ return; }
    
    Class toolClass = NSClassFromString(info.toolName);
    
    if(toolClass){
        id instance = [toolClass alloc];
        if(instance!=nil && [instance isKindOfClass:[ImageToolBase class]]){
            instance = [instance initWithImageEditor:self withToolInfo:info];
            self.currentTool = instance;
        }
    }
}

- (void)tappedMenuView:(UITapGestureRecognizer*)sender
{
    NSLog(@"tappedMenuView");
    
   
    [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"MainMenu"];
    
    UIView *view = sender.view;
    
    view.alpha = 0.2;
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         view.alpha = 1;
                     }
     ];
    
    [self setupToolWithToolInfo:view.toolInfo];
}

- (IBAction)pushedCancelBtn:(id)sender
{
    NSLog(@"pushedCancelBtn");
    
    [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"MainMenu"];
    
    _imageView.image = _originalImage;
    [self resetImageViewFrame];
    
    self.currentTool = nil;
    
    self.navTitleLabel.text = @"EDIT";
    
    self.cancleButton.hidden = YES;
    self.doneButton.hidden = YES;
    
    self.closeButton.hidden = NO;
    self.saveButton.hidden = NO;
}

- (IBAction)pushedDoneBtn:(id)sender
{
    self.view.userInteractionEnabled = NO;
    
    [self.currentTool executeWithCompletionBlock:^(UIImage *image, NSError *error, NSDictionary *userInfo) {
        if(error){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if(image){
            
            self->_originalImage = image;
            self->_imageView.image = image;
            
            NSLog(@"self->_imageView...... %@",NSStringFromCGRect(self->_imageView.frame));
            
            [self resetImageViewFrame];
            self.currentTool = nil;
        }
        self.view.userInteractionEnabled = YES;
        
        
        self.cancleButton.hidden = YES;
        self.doneButton.hidden = YES;
        
        self.closeButton.hidden = NO;
        self.saveButton.hidden = NO;
    }];
    
    
}

- (IBAction)closeButtonAction:(id)sender {
    
    if(self.targetImageView==nil){
        if([self.delegate respondsToSelector:@selector(imageEditorDidCancel:)]){
            [self.delegate imageEditorDidCancel:self];
        }
        else{
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    else{
        
        _imageView.image = self.targetImageView.image;
        [self restoreImageView:YES];
    }
}


//- (void)pushedCloseBtn:(id)sender
//{
//    if(self.targetImageView==nil){
//        if([self.delegate respondsToSelector:@selector(imageEditorDidCancel:)]){
//            [self.delegate imageEditorDidCancel:self];
//        }
//        else{
//            [self dismissViewControllerAnimated:YES completion:nil];
//        }
//    }
//    else{
//        _imageView.image = self.targetImageView.image;
//        [self restoreImageView:YES];
//    }
//}

- (IBAction)saveButtonAction:(id)sender {
    
    if(self.targetImageView==nil){
      
        [self didFinishEdittingWithImage:_originalImage];
        
    }
    else{

        _imageView.image = _originalImage;
        [self restoreImageView:NO];
    }
}


- (void)didFinishEdittingWithImage:(UIImage *)image {
    
    NSLog(@"didFinishEdittingWithImage");
    //add watermark
    
    UIImage *watermarkImage = [UIImage imageNamed:@"waterMark.png"];
    // watermarkImageView.image = watermarkImage;
    
    WaterMarkImageItem *imageItem = [[WaterMarkImageItem alloc] init];
    imageItem.watermarkImage = watermarkImage;
    imageItem.watermarkFrameOnOriginalImageView = CGRectMake(_imageView.frame.size.width - 50, _imageView.frame.size.height - 40, 30, 30);
    
    WaterMarkCreatorModel *model = [[WaterMarkCreatorModel alloc] init];
    model.originalImage = _originalImage;
    model.originalImageSize = _originalImage.size;
    model.originalImageViewSize = _imageView.bounds.size;
    model.itemArray = @[imageItem];
    model.successBlock = ^(WaterMarkCreatorModel *originItem, UIImage *processedImage, NSURL *processedVideoUrl) {
        
        if(processedImage)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
             //   _imageView.image = processedImage;
                
               // UIImageWriteToSavedPhotosAlbum(processedImage, nil, nil, nil);
                
                NSArray *items = @[processedImage];
                
                // build an activity view controller
                UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
                
                // and present it
                [self presentActivityController:controller];
                

                
            });
        }
    };
    [[[WaterMarkCreator alloc] init] createWatermarkWithCreatorModels:@[model]];

    
    
    
//
//    self.shareView.hidden = NO;
//    self.shareView.frame = CGRectMake(0,self.view.frame.size.height + 50 ,self.shareView.frame.size.width,self.shareView.frame.size.height);
//
//    [UIView animateWithDuration:.5
//                          delay:0
//                        options: UIViewAnimationOptionTransitionNone
//                     animations:^{
//
//
//
//                         self.shareView.frame = CGRectMake(0,(self.view.frame.size.height - self.shareView.frame.size.height) ,self.shareView.frame.size.width,self.shareView.frame.size.height);
//
//
//                     }
//
//                     completion:^(BOOL finished){
//
//
//                         // NSLog(@"frame: %@", NSStringFromCGRect(self.loginView.frame));
//
//                     }];
    
    
}


- (void)presentActivityController:(UIActivityViewController *)controller {
    
    // for iPad: make the presentation a Popover
    controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = self.navigationItem.leftBarButtonItem;
    
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        
        // react to the completion
        if (completed) {
            // user shared an item
            NSLog(@"We used activity type%@", activityType);
            
        } else {
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
        
        
        
        self.rateUsBgView.hidden = NO;
        self.rateUsView.hidden = NO;
        
        
    };
}

- (IBAction)cancleSaveButtonAction:(id)sender {
    
    [UIView animateWithDuration:.5
                          delay:0
                        options: UIViewAnimationOptionTransitionNone
                     animations:^{
                         
                         
                         
                         self.shareView.frame = CGRectMake(0,self.view.frame.size.height + 50 ,self.shareView.frame.size.width,self.shareView.frame.size.height);
                         
                         
                     }                     completion:^(BOOL finished){
                         
                         
                         self.shareView.hidden = YES;
                         
                     }];
    
    
}

#pragma mark- ScrollView delegate

//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
//{
//    NSLog(@"viewForZoomingInScrollView");
//    return _imageView;
//}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    CGFloat Ws = _scrollView.frame.size.width - _scrollView.contentInset.left - _scrollView.contentInset.right;
    CGFloat Hs = _scrollView.frame.size.height - _scrollView.contentInset.top - _scrollView.contentInset.bottom;
    CGFloat W = _imageView.frame.size.width;
    CGFloat H = _imageView.frame.size.height;
    
    CGRect rct = _imageView.frame;
    rct.origin.x = MAX((Ws-W)/2, 0);
    rct.origin.y = MAX((Hs-H)/2, 0);
    
    NSLog(@"scrollViewDidZoom");
    
//    CGFloat offsetY = 0;
//    if (_scrollView.zoomScale > 1){
//        offsetY = _scrollView.contentOffset.y;
//
//       [_scrollView setContentOffset: CGPointMake(_scrollView.contentOffset.x, offsetY)];
//    }
    
    _imageView.frame = rct;
    
    NSLog(@"scroll view.frame %@",NSStringFromCGRect(scrollView.frame));
    
    NSLog(@"_imageView.frame %@",NSStringFromCGRect(_imageView.frame));
    
   // [self centerScrollViewContents];
}

//- (void)centerScrollViewContents {
//
//    CGSize boundsSize = _scrollView.bounds.size;
//    CGRect contentsFrame = _imageView.frame;
//
//    if (contentsFrame.size.width < boundsSize.width) {
//        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
//    } else {
//        contentsFrame.origin.x = 0.0f;
//    }
//
//    if (contentsFrame.size.height < boundsSize.height) {
//        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
//    } else {
//        contentsFrame.origin.y = 0.0f;
//    }
//
//    _imageView.frame = contentsFrame;
//}


- (IBAction)rateUsButtonAction:(id)sender {
    
    [SKStoreReviewController requestReview];
    
    self.rateUsBgView.hidden = YES;
    self.rateUsView.hidden = YES;
    
}
- (IBAction)maybeLaterButtonAction:(id)sender {
    
    self.rateUsBgView.hidden = YES;
    self.rateUsView.hidden = YES;
}



@end
