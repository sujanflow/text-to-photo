//
//  FontCollectionViewCell.m
//  Pro Shot
//
//  Created by Tanvir Palash on 5/3/17.
//  Copyright © 2017 FV iMAGINATION. All rights reserved.
//

#import "FontCollectionViewCell.h"
#import "Configs.h"

@implementation FontCollectionViewCell

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor=[UIColor clearColor];
        
        if(!self.imageView)
        {
//            self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width , self.frame.size.height)];
//            self.label.textColor = [UIColor blackColor];
//            self.label.backgroundColor = [UIColor clearColor];
//            [self.label setFont:[UIFont systemFontOfSize:12]];
//            self.label.textAlignment = NSTextAlignmentCenter;
//            
              self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, self.frame.size.width-4 , self.frame.size.height-4)];
              self.imageView.contentMode = UIViewContentModeScaleAspectFit;
              [self.contentView addSubview:self.imageView];
            
            //[self.contentView addSubview:self.label];
            self.iapDot = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 7, 7)];
            self.iapDot.backgroundColor = PURPLE_COLOR;
            self.iapDot.layer.cornerRadius = self.iapDot.bounds.size.width/2;

            [self.contentView addSubview:self.iapDot];


        }
       
        
    }
    return self;
}


-(void)layoutIfNeeded
{

    [super layoutIfNeeded];
    //self.label.frame=CGRectMake(0, 0, self.frame.size.width , self.frame.size.height);
    self.imageView.frame=CGRectMake(2, 2, self.frame.size.width-4 , self.frame.size.height-4);

}

@end
