//
//  MatteFliterBase.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 26/5/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "MatteFilterBase.h"

#import <UIKit/UIKit.h>


@implementation MatteFilterBase



+ (NSString*)defaultIconImagePath
{
    return nil;
}

+ (NSArray*)subtools
{
    return nil;
}


+ (CGFloat)defaultDockedNumber
{
    return 0;
}


+ (NSString*)defaultTitle
{
    return @"MatteFilterBase";
}

+ (BOOL)isAvailable
{
    return NO;
}

+ (NSDictionary*)optionalInfo
{
    return nil;
}

#pragma mark-

+ (UIImage*)applyFilter:(UIImage*)image
{
    return image;
}

#pragma mark- FILTERS LIST ============================

@end


@interface MatteEmptyFilter : MatteFilterBase

@end

@implementation MatteEmptyFilter

+ (NSDictionary*)defaultFilterInfo
{
    NSDictionary *defaultFilterInfo = nil;
    if (defaultFilterInfo == nil){
        defaultFilterInfo =
        @{
          @"MatteEmptyFilter": @{@"name":@"MatteEmptyFilter",
                                   @"title":NSLocalizedString(@"MatteEmptyFilter", @""),
                                   @"version":@(0.0), @"dockedNum":@(0.0) },
          
          @"Matte1": @{@"name":@"CISepiaTone",
                         @"title":NSLocalizedString(@"Matte1", @""),
                         @"version":@(7.0), @"dockedNum":@(1.0)},
          
          @"Matte2": @{@"name":@"CISepiaTone",
                         @"title":NSLocalizedString(@"Matte2", @""),
                         @"version":@(7.0), @"dockedNum":@(2.0)},
          
          @"Matte3": @{@"name":@"CISepiaTone",
                         @"title":NSLocalizedString(@"Matte3", @""),
                         @"version":@(7.0), @"dockedNum":@(3.0)},
          @"Matte4": @{@"name":@"CISepiaTone",
                       @"title":NSLocalizedString(@"Matte4", @""),
                       @"version":@(7.0), @"dockedNum":@(4.0)},
          
          @"Matte5": @{@"name":@"CISepiaTone",
                       @"title":NSLocalizedString(@"Matte5", @""),
                       @"version":@(7.0), @"dockedNum":@(5.0)}
          
          
          
          };
    }
    return defaultFilterInfo;
}

+ (id)defaultInfoForKey:(NSString*)key {
    return self.defaultFilterInfo[NSStringFromClass(self)][key];
}

+ (NSString*)filterName {
    return [self defaultInfoForKey:@"name"];
}

+ (NSString*)defaultTitle {
    return [self defaultInfoForKey:@"title"];
}

+ (BOOL)isAvailable {
    return true;
    //([UIDevice iosVersion] >= [[self defaultInfoForKey:@"version"] floatValue]);
}

+ (CGFloat)defaultDockedNumber {
    return [[self defaultInfoForKey:@"dockedNum"] floatValue];
}

#pragma mark - FILTERS IMPLEMENTATION ======================================

+ (UIImage*)applyFilter:(UIImage *)image
{
    return [self title:self.defaultTitle  filteredImage:image withFilterName:self.filterName];
}

+ (UIImage*)title:(NSString*)title filteredImage:(UIImage*)image withFilterName:(NSString*)filterName {
    
    // No Filter ========================
    if([filterName isEqualToString:@"MatteEmptyFilter"]){
        return image;
    }
    
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
    
    // NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    
    if([title isEqualToString:@"Matte1"]){
        
        NSLog(@"Matte1");
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey, ciImage,@"inputIntensity", [NSNumber numberWithFloat:0.5], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
        
        
    }else if ([title isEqualToString:@"Matte2"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey, ciImage,@"inputIntensity", [NSNumber numberWithFloat:0.7], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        
        return result;
        
    }else if ([title isEqualToString:@"Matte3"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey, ciImage,@"inputIntensity", [NSNumber numberWithFloat:1], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
    }else if ([title isEqualToString:@"Matte4"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey, ciImage,@"inputIntensity", [NSNumber numberWithFloat:1.3], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
    }else if ([title isEqualToString:@"Matte5"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey, ciImage,@"inputIntensity", [NSNumber numberWithFloat:1.5], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
    }
    
    return nil;
    
}

@end


@interface MatteLinearFilter : MatteEmptyFilter
@end
@implementation MatteLinearFilter
@end

@interface Matte1 : MatteEmptyFilter
@end
@implementation Matte1
@end

@interface Matte2 : MatteEmptyFilter
@end
@implementation Matte2
@end

@interface Matte3 : MatteEmptyFilter
@end
@implementation Matte3
@end

@interface Matte4 : MatteEmptyFilter
@end
@implementation Matte4
@end

@interface Matte5 : MatteEmptyFilter
@end
@implementation Matte5
@end
