//
//  B&WFilterBase.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 17/5/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "B&WFilterBase.h"

@implementation B_WFilterBase


+ (NSString*)defaultIconImagePath
{
    return nil;
}

+ (NSArray*)subtools
{
    return nil;
}


+ (CGFloat)defaultDockedNumber
{
    return 0;
}


+ (NSString*)defaultTitle
{
    return @"BWFilterBase";
}

+ (BOOL)isAvailable
{
    return NO;
}

+ (NSDictionary*)optionalInfo
{
    return nil;
}

#pragma mark-

+ (UIImage*)applyFilter:(UIImage*)image
{
    return image;
}

#pragma mark- FILTERS LIST ============================

@end
@interface BWEmptyFilter : B_WFilterBase

@end

@implementation BWEmptyFilter

+ (NSDictionary*)defaultFilterInfo
{
    NSDictionary *defaultFilterInfo = nil;
    if (defaultFilterInfo == nil){
        defaultFilterInfo =
        
        @{
         @"BWEmptyFilter": @{@"name":@"BWEmptyFilter",
                               @"title":NSLocalizedString(@"BWEmptyFilter", @""),
                               @"version":@(0.0), @"dockedNum":@(0.0) },
        
        @"BW1": @{@"name":@"CIPhotoEffectMono",
                                  @"title":NSLocalizedString(@"10", @""),
                                  @"version":@(7.0), @"dockedNum":@(1.0)},
          
          @"BW2": @{@"name":@"CIPhotoEffectNoir",
                                    @"title":NSLocalizedString(@"9", @""),
                                    @"version":@(7.0), @"dockedNum":@(2.0)},
          
          @"BW3": @{@"name":@"CIPhotoEffectTonal",
                               @"title":NSLocalizedString(@"8", @""),
                               @"version":@(7.0), @"dockedNum":@(3.0)},
          @"BW4": @{@"name":@"BW4",
                   @"title":NSLocalizedString(@"7", @""),
                   @"version":@(7.0), @"dockedNum":@(4.0)},
         @"BW5": @{@"name":@"BW5",
                   @"title":NSLocalizedString(@"6", @""),
                   @"version":@(7.0), @"dockedNum":@(5.0)},
         @"BW5": @{@"name":@"BW5",
                   @"title":NSLocalizedString(@"5", @""),
                   @"version":@(7.0), @"dockedNum":@(6.0)},
         @"BW6": @{@"name":@"BW6",
                   @"title":NSLocalizedString(@"4", @""),
                   @"version":@(7.0), @"dockedNum":@(7.0)},
         @"BW7": @{@"name":@"BW7",
                   @"title":NSLocalizedString(@"3", @""),
                   @"version":@(7.0), @"dockedNum":@(8.0)},
         @"BW8": @{@"name":@"BW8",
                   @"title":NSLocalizedString(@"2", @""),
                   @"version":@(7.0), @"dockedNum":@(9.0)},
         @"BW9": @{@"name":@"BW9",
                   @"title":NSLocalizedString(@"1", @""),
                   @"version":@(7.0), @"dockedNum":@(10.0)}
          
          
          };
    }
    return defaultFilterInfo;
}

+ (id)defaultInfoForKey:(NSString*)key {
    return self.defaultFilterInfo[NSStringFromClass(self)][key];
}

+ (NSString*)filterName {
    return [self defaultInfoForKey:@"name"];
}

+ (NSString*)defaultTitle {
    return [self defaultInfoForKey:@"title"];
}

+ (BOOL)isAvailable {
    return true;
    //([UIDevice iosVersion] >= [[self defaultInfoForKey:@"version"] floatValue]);
}

+ (CGFloat)defaultDockedNumber {
    return [[self defaultInfoForKey:@"dockedNum"] floatValue];
}

#pragma mark - FILTERS IMPLEMENTATION ======================================

+ (UIImage*)applyFilter:(UIImage *)image
{
    return [self filteredImage:image withFilterName:self.filterName];
}

+ (UIImage*)filteredImage:(UIImage*)image withFilterName:(NSString*)filterName {
    
    // No Filter ========================
    if([filterName isEqualToString:@"BWEmptyFilter"]){
        return image;
    }
    
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
    
    // NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    
    

    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    
    if ([filterName isEqualToString:@"BW4"]) {
        
        
        UIImage *result =   [self processUsingPixels:image colorValue:2.0];
       
        return result;

    }else if ([filterName isEqualToString:@"BW5"]) {
        
        
        UIImage *result =   [self processUsingPixels:image colorValue:3.0];
        
        return result;
        
    }else if ([filterName isEqualToString:@"BW6"]) {
        
        
        UIImage *result =   [self processUsingPixels:image colorValue:4.0];
        
        return result;
        
    }else if ([filterName isEqualToString:@"BW7"]) {
        
        
        UIImage *result =   [self processUsingPixels:image colorValue:5.0];
        
        return result;
        
    }else if ([filterName isEqualToString:@"BW8"]) {
        
        UIImage *result =   [self processUsingPixels:image colorValue:6.0];
        
        return result;
        
    }else if ([filterName isEqualToString:@"BW9"]) {
        
        
        UIImage *result =   [self processUsingPixels:image colorValue:7.0];
        
        return result;
        
    }




    

    
    return result;
}




#pragma mark - Private

#define Mask8(x) ( (x) & 0xFF )
#define R(x) ( Mask8(x) )
#define G(x) ( Mask8(x >> 8 ) )
#define B(x) ( Mask8(x >> 16) )
#define A(x) ( Mask8(x >> 24) )
#define RGBAMake(r, g, b, a) ( Mask8(r) | Mask8(g) << 8 | Mask8(b) << 16 | Mask8(a) << 24 )
+ (UIImage *)processUsingPixels:(UIImage*)inputImage colorValue:(float)colorInt {
    
    // 1. Get the raw pixels of the image
    UInt32 * inputPixels;
    
    CGImageRef inputCGImage = [inputImage CGImage];
    NSUInteger inputWidth = CGImageGetWidth(inputCGImage);
    NSUInteger inputHeight = CGImageGetHeight(inputCGImage);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    NSUInteger bytesPerPixel = 4;
    NSUInteger bitsPerComponent = 8;
    
    NSUInteger inputBytesPerRow = bytesPerPixel * inputWidth;
    
    inputPixels = (UInt32 *)calloc(inputHeight * inputWidth, sizeof(UInt32));
    
    CGContextRef context = CGBitmapContextCreate(inputPixels, inputWidth, inputHeight,
                                                 bitsPerComponent, inputBytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, inputWidth, inputHeight), inputCGImage);
    
    
    // 3. Convert the image to Black & White
    for (NSUInteger j = 0; j < inputHeight; j++) {
        for (NSUInteger i = 0; i < inputWidth; i++) {
            UInt32 * currentPixel = inputPixels + (j * inputWidth) + i;
            UInt32 color = *currentPixel;
            
            // Average of RGB = greyscale
            UInt32 averageColor = (R(color) + G(color) + B(color)) / colorInt;
            
            *currentPixel = RGBAMake(averageColor, averageColor, averageColor, A(color));
        }
    }
    
    // 4. Create a new UIImage
    CGImageRef newCGImage = CGBitmapContextCreateImage(context);
    UIImage * processedImage = [UIImage imageWithCGImage:newCGImage];
    
    // 5. Cleanup!
    //  CGColorSpaceRelease(colorSpace);
    //  CGContextRelease(context);
    //  CGContextRelease(ghostContext);
    //  free(inputPixels);
    //  free(ghostPixels);
    
    return processedImage;
}

@end


@interface BW1 : BWEmptyFilter
@end
@implementation BW1
@end

@interface BW2 : BWEmptyFilter
@end
@implementation BW2
@end

@interface BW3 : BWEmptyFilter
@end
@implementation BW3
@end
@interface BW4 : BWEmptyFilter
@end
@implementation BW4
@end
@interface BW5 : BWEmptyFilter
@end
@implementation BW5
@end
@interface BW6 : BWEmptyFilter
@end
@implementation BW6
@end
@interface BW7 : BWEmptyFilter
@end
@implementation BW7
@end
@interface BW8 : BWEmptyFilter
@end
@implementation BW8
@end
@interface BW9 : BWEmptyFilter
@end
@implementation BW9
@end

