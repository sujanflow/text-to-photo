/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import "FilterTool.h"
#import "FilterBase.h"
#import "Configs.h"
#import "_CLImageEditorViewController.h"
#import "BasicFilterBase.h"
#import "B&WFilterBase.h"
#import "SummerFliterBase.h"
#import "DuoToneFliterBase.h"
#import "VintageFilterBase.h"
#import "MatteFilterBase.h"
#import "InvertFilterBase.h"


//#import "IAPController.h"

static NSString* const kStickerToolStickerPathKey = @"stickerPath";


@interface FilterTool()


@end

@implementation FilterTool
{
    UIImage *_originalImage;
    UIScrollView *_menuScroll;
    UIScrollView *_submenuScroll;
    
    // For IAP =========
    UIImageView *iapDot;
   // NSTimer *iapTimer;
    int tagINT,
    filterTAG;
    
    BOOL iapMade;
    
    BOOL iapForEffects;

    int selectedFilterBase;
}

+ (NSArray*)subtools
{
    NSLog(@"subtools.....");
    return [ImageToolInfo toolsWithToolClass:[FilterBase class]];
}


+ (NSString*)defaultTitle
{
    return NSLocalizedString(@"Filter", @"");
}

+ (BOOL)isAvailable
{
    return true;
}

- (void)setup {
    
    selectedFilterBase = 0;
    
    _originalImage = self.editor.imageView.image;
    
    // Fire IAP timer
    //iapTimer = [NSTimer scheduledTimerWithTimeInterval:0.2  target:self selector:@selector(removeIapDots:)  userInfo:nil repeats:true];
    tagINT = 0;
    filterTAG = -1;
    
    
    _menuScroll = [[UIScrollView alloc] initWithFrame:self.editor.menuView.frame];
    _menuScroll.backgroundColor = self.editor.menuView.backgroundColor;
    _menuScroll.showsHorizontalScrollIndicator = NO;
    [self.editor.view addSubview:_menuScroll];
    
    _submenuScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0,self.editor.menuView.frame.origin.y-self.editor.menuView.frame.size.height, self.editor.menuView.frame.size.width,self.editor.menuView.frame.size.height)];
    _submenuScroll.backgroundColor = self.editor.menuView.backgroundColor;
    _submenuScroll.showsHorizontalScrollIndicator = NO;
    [self.editor.view addSubview:_submenuScroll];

    _submenuScroll.hidden = YES;
    
    
    // Fire IAP timer
    
    tagINT = 0;
    
    [self setMainFilterMenu];
    
    _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height-_menuScroll.top);
    [UIView animateWithDuration:kImageToolAnimationDuration animations:^{
        _menuScroll.transform = CGAffineTransformIdentity;
    }];
}

- (void)cleanup {
   //  [iapTimer invalidate];
    
    [UIView animateWithDuration:kImageToolAnimationDuration animations:^{
        _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height-_menuScroll.top);
    } completion:^(BOOL finished) {
        [_menuScroll removeFromSuperview];
        [_submenuScroll removeFromSuperview];
    }];
}

#pragma mark - REMOVE IAP DOTS METHOD  =================
-(void)refreshViewAfterInApp
{
    NSLog(@"refreshViewAfterInApp");
    [self removeIapDots];
//    if(isAdRemoved)
//        _menuScroll.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height-55 , [UIScreen mainScreen].bounds.size.width , 55);
    
}
// Remove iapDots icons from items that have been purchased with IAP
-(void)removeIapDots {
    
    
    if (iapForEffects || iapMade) {
        for (int i = 300+freeFilters; i <= iapDot.tag; i++) {
            [[self.editor.view viewWithTag:i] removeFromSuperview];
        }
       // [iapTimer invalidate];
    }
    // NSLog(@"timerON!");
}

- (void)executeWithCompletionBlock:(void (^)(UIImage *, NSError *, NSDictionary *))completionBlock {
    completionBlock(self.editor.imageView.image, nil, nil);
}



#pragma mark - SETUP FILTERS MENU ================================
- (void)setMainFilterMenu {
    
    CGFloat W = 70;
    CGFloat x = 0;
    
  //  UIImage *iconThumnail = [_originalImage aspectFill:CGSizeMake(50, 50)];
    
    
    NSString *stickerPath = [[self class] defaultFilterPath];
    
    if(stickerPath==nil){ stickerPath = [[self class] defaultFilterPath]; }
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error = nil;
    NSArray *list = [fileManager contentsOfDirectoryAtPath:stickerPath error:&error];
    
   // NSLog(@"list.....%@",stickerPath);
    NSArray *filterName = [[NSArray alloc]initWithObjects:@"Basic",@"B&W",@"Summer",@"Vintage",@"Duo-tone",@"Matte",@"Invert",nil];
    
    for(NSString *path in list){
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", stickerPath, path];
        NSString *SubMenuName = [NSString stringWithFormat:@"%@", path];
        UIImage *image = [UIImage imageWithContentsOfFile:filePath];
        
        if(image){
            ToolbarMenuItem *item =  [ImageEditorTheme menuItemWithFrame:CGRectMake(x, 0, W, _menuScroll.height) target:self action:@selector(tappedMainFilterPanel:) toolInfo:nil];
            
            tagINT++;
            item.tag = tagINT;
            
            NSLog(@"item.tag %ld",(long)item.tag);

            
            item.iconImage = [image aspectFit:CGSizeMake(50, 50)];
            
            item.userInfo = @{@"filePath" : SubMenuName};
            
           // NSLog(@"item.userInfo %@",item.userInfo);
            
            item.title = [filterName objectAtIndex:item.tag-1];
            
            NSLog(@"summer %d",[[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeSummerPack"]);
            

            
            if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeSummerPack"] && [item.title isEqualToString:@"Summer"]) {
                
                iapDot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 10, 10)];
                iapDot.image = [UIImage imageNamed:@"lock_white"];
                iapDot.backgroundColor = [UIColor clearColor];
                iapDot.layer.cornerRadius = iapDot.bounds.size.width/2;
                [item addSubview:iapDot];

            }else if ((![[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeB&W"] && [item.title isEqualToString:@"B&W"])){
                
                iapDot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 10, 10)];
                iapDot.image = [UIImage imageNamed:@"lock_white"];
                iapDot.backgroundColor = [UIColor clearColor];
                iapDot.layer.cornerRadius = iapDot.bounds.size.width/2;
                [item addSubview:iapDot];
            }else if ((![[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeInvert"] && [item.title isEqualToString:@"Invert"])){
                
                iapDot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 10, 10)];
                iapDot.image = [UIImage imageNamed:@"lock_white"];
                iapDot.backgroundColor = [UIColor clearColor];
                iapDot.layer.cornerRadius = iapDot.bounds.size.width/2;
                [item addSubview:iapDot];
            }else if ((![[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeDuo"] && [item.title isEqualToString:@"Duo-tone"])){
                
                iapDot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 10, 10)];
                iapDot.image = [UIImage imageNamed:@"lock_white"];
                iapDot.backgroundColor = [UIColor clearColor];
                iapDot.layer.cornerRadius = iapDot.bounds.size.width/2;
                [item addSubview:iapDot];
            }else if ((![[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeVintage"] && [item.title isEqualToString:@"Vintage"])){
                
                iapDot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 10, 10)];
                iapDot.image = [UIImage imageNamed:@"lock_white"];
                iapDot.backgroundColor = [UIColor clearColor];
                iapDot.layer.cornerRadius = iapDot.bounds.size.width/2;
                [item addSubview:iapDot];
            }else if ((![[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeMatt"] && [item.title isEqualToString:@"Matte"])){
                
                iapDot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 10, 10)];
                iapDot.image = [UIImage imageNamed:@"lock_white"];
                iapDot.backgroundColor = [UIColor clearColor];
                iapDot.layer.cornerRadius = iapDot.bounds.size.width/2;
                [item addSubview:iapDot];
            }
         
            
//            if (!iapForEffects && !iapMade && item.tag >= freeFilters) {
//
//                iapDot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 10, 10)];
//                iapDot.image = [UIImage imageNamed:@"lock_white"];
//                iapDot.backgroundColor = [UIColor clearColor];
//                iapDot.layer.cornerRadius = iapDot.bounds.size.width/2;
//                iapDot.tag = tagINT+600;
//                [item addSubview:iapDot];
//                //NSLog(@"iapDot TAG: %ld", (long)iapDot.tag);
//            }

            
            [_menuScroll addSubview:item];
            x += W;
        }
    }
    _menuScroll.contentSize = CGSizeMake(MAX(x, _menuScroll.frame.size.width+1), 0);
    
    
    
}
- (void)setFilterMenu {
    
    for (UIView *v in [_submenuScroll subviews]) {
        
        [v removeFromSuperview];
    }
    
    CGFloat W = 70;
    CGFloat x = 0;
    
    UIImage *iconThumnail = [_originalImage aspectFill:CGSizeMake(50, 50)];
    
    NSLog(@"selectedFilterBase in setFilterMenu %d",selectedFilterBase);
    
    NSArray *subTool = [ImageToolInfo toolsWithToolClass:[FilterBase class]];
    
    if (selectedFilterBase == 1) {
        
        subTool = [ImageToolInfo toolsWithToolClass:[BasicFilterBase class]];
        
    }else if (selectedFilterBase == 3){
        
        subTool = [ImageToolInfo toolsWithToolClass:[B_WFilterBase class]];
        
    }else if (selectedFilterBase == 2){
        
        subTool = [ImageToolInfo toolsWithToolClass:[SummerFilterBase class]];
        
    }else if (selectedFilterBase == 4){
        
        subTool = [ImageToolInfo toolsWithToolClass:[VintageFilterBase class]];
        
    }else if (selectedFilterBase == 5){
        
        subTool = [ImageToolInfo toolsWithToolClass:[DuoToneFilterBase class]];
    }else if (selectedFilterBase == 6){
        
        subTool = [ImageToolInfo toolsWithToolClass:[MatteFilterBase class]];
    }else if (selectedFilterBase == 7){
        
        subTool = [ImageToolInfo toolsWithToolClass:[InvertFilterBase class]];
    }
    
    for(ImageToolInfo *info in subTool){
        
        if(!info.available){
            continue;
        }
        
        NSLog(@" ImageToolInfo %@",info);
        
        ToolbarMenuItem *item = [ImageEditorTheme menuItemWithFrame:CGRectMake(x, 0, W, _submenuScroll.height) target:self action:@selector(tappedFilterPanel:) toolInfo:info];

        [_submenuScroll addSubview:item];
        //item.userInfo = @{@"filePath" : filePath};
        x += W;
        
        if(item.iconImage==nil){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                UIImage *iconImage = [self filteredImage:iconThumnail withToolInfo:info];
                [item performSelectorOnMainThread:@selector(setIconImage:) withObject:iconImage waitUntilDone:NO];
            });
        }
    }
    _submenuScroll.contentSize = CGSizeMake(MAX(x, _submenuScroll.frame.size.width+1), 0);
    

}


- (void)tappedMainFilterPanel:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
    NSString *SubMenuName = view.userInfo[@"filePath"];
    
    NSLog(@"tappedMainFilterPanel: %ld",sender.view.tag);
        
    
    if (sender.view.tag == 1) {
    
        selectedFilterBase = 1;
        self.editor.doneButton.hidden = NO;
        
    }else if (sender.view.tag == 2){
        
        selectedFilterBase = 3;
        
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeB&W"]) {
            
            self.editor.doneButton.hidden = YES;
        }else{
            self.editor.doneButton.hidden = NO;
        }

        
    }else if (sender.view.tag == 3){
        
        selectedFilterBase = 2;
        
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeSummerPack"]) {

            self.editor.doneButton.hidden = YES;
        }else{
            self.editor.doneButton.hidden = NO;
        }
        
    }else if (sender.view.tag == 4){
        
        selectedFilterBase = 4;
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeVintage"]) {
            
            self.editor.doneButton.hidden = YES;
        }else{
            self.editor.doneButton.hidden = NO;
        }

    }else if (sender.view.tag == 5){
        
        selectedFilterBase = 5;
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeDuo"]) {
            
            self.editor.doneButton.hidden = YES;
        }else{
            self.editor.doneButton.hidden = NO;
        }

    }else if (sender.view.tag == 6){
        
        selectedFilterBase = 6;
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeMatt"]) {
            
            self.editor.doneButton.hidden = YES;
        }else{
            self.editor.doneButton.hidden = NO;
        }

    }else if (sender.view.tag == 7){
        
        selectedFilterBase = 7;
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isIapMadeInvert"]) {
            
            self.editor.doneButton.hidden = YES;
        }else{
            self.editor.doneButton.hidden = NO;
        }

    }
    
   
    _submenuScroll.hidden = NO;
    
    
    [self setFilterMenu];
    
}
- (void)tappedFilterPanel:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
    NSLog(@"tag: %ld", (long)view.tag);
    view.alpha = 0.2;
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         view.alpha = 1;
                     }
     ];

        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            UIImage *image = [self filteredImage:_originalImage withToolInfo:view.toolInfo];
            
            [self.editor.imageView performSelectorOnMainThread:@selector(setImage:) withObject:image waitUntilDone:NO];
        });
        
        filterTAG++;
        view.tag = filterTAG;
    
}

- (UIImage*)filteredImage:(UIImage*)image withToolInfo:(ImageToolInfo*)info
{
    NSLog(@"selectedFilterBase in filteredImage %d",selectedFilterBase);
    if (selectedFilterBase == 1) {
        
        @autoreleasepool {
            Class filterClass = NSClassFromString(info.toolName);
            if([(Class)filterClass conformsToProtocol:@protocol(BasicFilterBaseProtocol)]){
                return [filterClass applyFilter:image];
            }
            return nil;
        }

    }else if (selectedFilterBase == 2){
        
        @autoreleasepool {
            Class filterClass = NSClassFromString(info.toolName);
            if([(Class)filterClass conformsToProtocol:@protocol(SummerFilterBaseProtocol)]){
                return [filterClass applyFilter:image];
            }
            return nil;
        }
    }else if (selectedFilterBase == 3){
        
        @autoreleasepool {
            Class filterClass = NSClassFromString(info.toolName);
            if([(Class)filterClass conformsToProtocol:@protocol(BWFilterBaseProtocol)]){
                return [filterClass applyFilter:image];
            }
            return nil;
        }

        
    }else if (selectedFilterBase == 4){
        
        @autoreleasepool {
            Class filterClass = NSClassFromString(info.toolName);
            if([(Class)filterClass conformsToProtocol:@protocol(VintageFilterBaseProtocol)]){
                return [filterClass applyFilter:image];
            }
            return nil;
        }
        
        
    }else if (selectedFilterBase == 5){
        
        @autoreleasepool {
            Class filterClass = NSClassFromString(info.toolName);
            if([(Class)filterClass conformsToProtocol:@protocol(DuoToneFilterBaseProtocol)]){
                return [filterClass applyFilter:image];
            }
            return nil;
        }
        
        
    }else if (selectedFilterBase == 6){
        
        @autoreleasepool {
            Class filterClass = NSClassFromString(info.toolName);
            if([(Class)filterClass conformsToProtocol:@protocol(MatteFilterBaseProtocol)]){
                return [filterClass applyFilter:image];
            }
            return nil;
        }
        
        
    }else if (selectedFilterBase == 7){
        
        @autoreleasepool {
            Class filterClass = NSClassFromString(info.toolName);
            if([(Class)filterClass conformsToProtocol:@protocol(InvertFilterBaseProtocol)]){
                return [filterClass applyFilter:image];
            }
            return nil;
        }
        
        
    }


    return nil;
}

#pragma mark- STICKER PATHS ============
// Default Stickers Path ===============
+ (NSString*)defaultFilterPath
{
    // return [[[ImageEditorTheme bundle] bundlePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", NSStringFromClass(self)]];
    return [[[ImageEditorTheme bundle] bundlePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/Filters", NSStringFromClass(self)]];
    
}

+ (NSString*)defaultStickerPathThumb
{
    //return [[[ImageEditorTheme bundle] bundlePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/stickers/thumb", NSStringFromClass(self)]];
    return [[[ImageEditorTheme bundle] bundlePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/Filters/", NSStringFromClass(self)]];
}
+ (NSDictionary*)optionalInfo
{
    return @{kStickerToolStickerPathKey:[self defaultFilterPath]};
}
//========================================



@end
