//
//  SummerFliterBase.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 26/5/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "SummerFliterBase.h"
#import <Accelerate/Accelerate.h>

void rgbToHSV(float rgb[3], float hsv[3])
{
    float min, max, delta;
    float r = rgb[0], g = rgb[1], b = rgb[2];
    //float *h = hsv[0], *s = hsv[1], *v = hsv[2];
    
    min = MIN( r, MIN( g, b ));
    max = MAX( r, MAX( g, b ));
    hsv[2] = max;               // v
    delta = max - min;
    if( max != 0 )
        hsv[1] = delta / max;       // s
    else {
        // r = g = b = 0        // s = 0, v is undefined
        hsv[1] = 0;
        hsv[0] = -1;
        return;
    }
    if( r == max )
        hsv[0] = ( g - b ) / delta;     // between yellow & magenta
    else if( g == max )
        hsv[0] = 2 + ( b - r ) / delta; // between cyan & yellow
    else
        hsv[0] = 4 + ( r - g ) / delta; // between magenta & cyan
    hsv[0] *= 60;               // degrees
    if( hsv[0] < 0 )
        hsv[0] += 360;
    hsv[0] /= 360.0;
}

void hsvToRGB(float hsv[3], float rgb[3])
{
    float C = hsv[2] * hsv[1];
    float HS = hsv[0] * 6.0;
    float X = C * (1.0 - fabsf(fmodf(HS, 2.0) - 1.0));
    
    if (HS >= 0 && HS < 1)
    {
        rgb[0] = C;
        rgb[1] = X;
        rgb[2] = 0;
    }
    else if (HS >= 1 && HS < 2)
    {
        rgb[0] = X;
        rgb[1] = C;
        rgb[2] = 0;
    }
    else if (HS >= 2 && HS < 3)
    {
        rgb[0] = 0;
        rgb[1] = C;
        rgb[2] = X;
    }
    else if (HS >= 3 && HS < 4)
    {
        rgb[0] = 0;
        rgb[1] = X;
        rgb[2] = C;
    }
    else if (HS >= 4 && HS < 5)
    {
        rgb[0] = X;
        rgb[1] = 0;
        rgb[2] = C;
    }
    else if (HS >= 5 && HS < 6)
    {
        rgb[0] = C;
        rgb[1] = 0;
        rgb[2] = X;
    }
    else {
        rgb[0] = 0.0;
        rgb[1] = 0.0;
        rgb[2] = 0.0;
    }
    
    
    float m = hsv[2] - C;
    rgb[0] += m;
    rgb[1] += m;
    rgb[2] += m;
}



@implementation SummerFilterBase



+ (NSString*)defaultIconImagePath
{
    return nil;
}

+ (NSArray*)subtools
{
    return nil;
}


+ (CGFloat)defaultDockedNumber
{
    return 0;
}


+ (NSString*)defaultTitle
{
    return @"SummerFilterBase";
}

+ (BOOL)isAvailable
{
    return NO;
}

+ (NSDictionary*)optionalInfo
{
    return nil;
}

#pragma mark-

+ (UIImage*)applyFilter:(UIImage*)image
{
    return image;
}

#pragma mark- FILTERS LIST ============================

@end
@interface SummerEmptyFilter : SummerFilterBase

@end

@implementation SummerEmptyFilter

+ (NSDictionary*)defaultFilterInfo
{
    NSDictionary *defaultFilterInfo = nil;
    if (defaultFilterInfo == nil){
        defaultFilterInfo =
        @{
          @"SummerEmptyFilter": @{@"name":@"SummerEmptyFilter",
                                 @"title":NSLocalizedString(@"SummerEmptyFilter", @""),
                                 @"version":@(0.0), @"dockedNum":@(0.0) },
          
          @"Summer6": @{@"name":@"CIColorCube",
                                  @"title":NSLocalizedString(@"Summer1", @""),
                                  @"version":@(7.0), @"dockedNum":@(1.0)},
          
          @"Summer5": @{@"name":@"CIColorCube",
                                    @"title":NSLocalizedString(@"Summer2", @""),
                                    @"version":@(7.0), @"dockedNum":@(2.0)},
          
          @"Summer4": @{@"name":@"CIColorCube",
                               @"title":NSLocalizedString(@"Summer3", @""),
                               @"version":@(7.0), @"dockedNum":@(3.0)},
          
          @"Summer3": @{@"name":@"CIColorCube",
                        @"title":NSLocalizedString(@"Summer4", @""),
                        @"version":@(7.0), @"dockedNum":@(4.0)},
          
          @"Summer2": @{@"name":@"CIColorCube",
                        @"title":NSLocalizedString(@"Summer5", @""),
                        @"version":@(7.0), @"dockedNum":@(5.0)},
          
          @"Summer1": @{@"name":@"CIColorCube",
                        @"title":NSLocalizedString(@"Summer6", @""),
                        @"version":@(7.0), @"dockedNum":@(6.0)}
          
         
          
          };
    }
    
  //  NSLog(@"defaultFilterInfo %@",defaultFilterInfo);
    return defaultFilterInfo;
}

+ (id)defaultInfoForKey:(NSString*)key {
    return self.defaultFilterInfo[NSStringFromClass(self)][key];
}

+ (NSString*)filterName {
    return [self defaultInfoForKey:@"name"];
}

+ (NSString*)defaultTitle {
    return [self defaultInfoForKey:@"title"];
}

+ (BOOL)isAvailable {
    return true;
    //([UIDevice iosVersion] >= [[self defaultInfoForKey:@"version"] floatValue]);
}

+ (CGFloat)defaultDockedNumber {
    return [[self defaultInfoForKey:@"dockedNum"] floatValue];
}

#pragma mark - FILTERS IMPLEMENTATION ======================================

+ (UIImage*)applyFilter:(UIImage *)image
{
    return [self title:self.defaultTitle  filteredImage:image withFilterName:self.filterName];
}

+ (UIImage*)title:(NSString*)title filteredImage:(UIImage*)image withFilterName:(NSString*)filterName {
    
    // No Filter ========================
    if([filterName isEqualToString:@"SummerEmptyFilter"]){
        return image;
    }
    
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
    
    // NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    
    // CIVignette Filter ===============
    if([title isEqualToString:@"Summer1"]){
        
        NSLog(@"Summer1");
        // parameters
      
        CIFilter *colorCube = [CIFilter filterWithName:@"CIPhotoEffectChrome"];
        [colorCube setValue:ciImage forKey:kCIInputImageKey];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [colorCube outputImage];
        
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        
        return result;

    }else if ([title isEqualToString:@"Summer2"]){
        
        NSLog(@"Summer2");
        // parameters
       
        
        CIFilter *colorCube = [CIFilter filterWithName:@"CIPhotoEffectInstant"];
        
        [colorCube setValue:ciImage forKey:kCIInputImageKey];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [colorCube outputImage];
        
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        
        return result;
        
    }else if ([title isEqualToString:@"Summer3"]){
        
        NSLog(@"Summer3");
        // parameters
        CIFilter *clFilter = [CIFilter filterWithName:@"CIExposureAdjust"];
        [clFilter setValue:ciImage forKey:@"inputImage"];
        [clFilter setValue:[NSNumber numberWithInteger:2] forKey:@"inputEV"];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [clFilter outputImage];
        
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        
        return result;

        
    }else if ([title isEqualToString:@"Summer4"]){
        
       
        // parameters
        
        CIFilter *clFilter = [CIFilter filterWithName:@"CIColorControls"];
        [clFilter setValue:ciImage forKey:@"inputImage"];
        [clFilter setValue:[NSNumber numberWithInteger:1] forKey:@"inputSaturation"];
        [clFilter setValue:[NSNumber numberWithInteger:1.2] forKey:@"inputBrightness"];
        [clFilter setValue:[NSNumber numberWithInteger:3] forKey:@"inputContrast"];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [clFilter outputImage];
        
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        
        return result;
        
    }else if ([title isEqualToString:@"Summer5"]){
        
        
        // parameters
        
        CIFilter *clFilter = [CIFilter filterWithName:@"CIPhotoEffectProcess"];
        
        [clFilter setValue:ciImage forKey:@"inputImage"];
        
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [clFilter outputImage];
        
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        
        return result;

        
    }else if ([title isEqualToString:@"Summer6"]){
        
        
        // parameters
        
        CIFilter *clFilter = [CIFilter filterWithName:@"CIExposureAdjust"];
        [clFilter setValue:ciImage forKey:@"inputImage"];
        [clFilter setValue:[NSNumber numberWithInteger:1.3] forKey:@"inputEV"];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [clFilter outputImage];
        
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        
        return result;
        
    }
    
    return nil;
   
}
//
//+ (nullable NSData *) colorCubeDataFromLUT:(UIImage *)image
//{
////    UIImage *image = [UIImage imageNamed:name inBundle:[NSBundle bundleForClass:self.class] compatibleWithTraitCollection:nil];
//    static const int kDimension = 64;
//
//    if (!image) return nil;
//
//    NSInteger width = CGImageGetWidth(image.CGImage);
//    NSInteger height = CGImageGetHeight(image.CGImage);
//    NSInteger rowNum = height / kDimension;
//    NSInteger columnNum = width / kDimension;
//
//    if ((width % kDimension != 0) || (height % kDimension != 0) || (rowNum * columnNum != kDimension)) {
//      //  NSLog(@"Invalid colorLUT %@",name);
//        return nil;
//    }
//
//    float *bitmap = [self createRGBABitmapFromImage:image.CGImage];
//    if (bitmap == NULL) return nil;
//
//    // Convert bitmap data written in row,column order to cube data written in x:r, y:g, z:b representation where z varies > y varies > x.
//    NSInteger size = kDimension * kDimension * kDimension * sizeof(float) * 4;
//    float *data = malloc(size);
//    int bitmapOffset = 0;
//    int z = 0;
//    for (int row = 0; row <  rowNum; row++)
//    {
//        for (int y = 0; y < kDimension; y++)
//        {
//            int tmp = z;
//            for (int col = 0; col < columnNum; col++) {
//                NSInteger dataOffset = (z * kDimension * kDimension + y * kDimension) * 4;
//
//                const float divider = 255.0;
//                vDSP_vsdiv(&bitmap[bitmapOffset], 1, &divider, &data[dataOffset], 1, kDimension * 4); // Vector scalar divide; single precision. Divides bitmap values by 255.0 and puts them in data, processes each column (kDimension * 4 values) at once.
//
//                bitmapOffset += kDimension * 4; // shift bitmap offset to the next set of values, each values vector has (kDimension * 4) values.
//                z++;
//            }
//            z = tmp;
//        }
//        z += columnNum;
//    }
//
//    free(bitmap);
//
//    return [NSData dataWithBytesNoCopy:data length:size freeWhenDone:YES];
//}
//
//+ (float *)createRGBABitmapFromImage:(CGImageRef)image {
//    CGContextRef context = NULL;
//    CGColorSpaceRef colorSpace;
//    unsigned char *bitmap;
//    NSInteger bitmapSize;
//    NSInteger bytesPerRow;
//
//    size_t width = CGImageGetWidth(image);
//    size_t height = CGImageGetHeight(image);
//
//    bytesPerRow   = (width * 4);
//    bitmapSize     = (bytesPerRow * height);
//
//    bitmap = malloc( bitmapSize );
//    if (bitmap == NULL) return NULL;
//
//    colorSpace = CGColorSpaceCreateDeviceRGB();
//    if (colorSpace == NULL) {
//        free(bitmap);
//        return NULL;
//    }
//    context = CGBitmapContextCreate (bitmap,
//                                     width,
//                                     height,
//                                     8,
//                                     bytesPerRow,
//                                     colorSpace,
//                                     (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
//    CGColorSpaceRelease( colorSpace );
//
//    if (context == NULL) {
//        free (bitmap);
//        return NULL;
//    }
//
//    CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
//    CGContextRelease(context);
//
//    float *convertedBitmap = malloc(bitmapSize * sizeof(float));
//    vDSP_vfltu8(bitmap, 1, convertedBitmap, 1, bitmapSize); // Converts an array of unsigned 8-bit integers to single-precision floating-point values.
//    free(bitmap);
//
//    return convertedBitmap;
//}


//+ (NSData*)render: (float)minHue :(float)maxHue
//{
//    float minHueAngle = minHue;
//    float maxHueAngle = maxHue;
//    float centerHueAngle = minHueAngle + (maxHueAngle - minHueAngle)/2.0;
//    float destCenterHueAngle = 1.0/1.50;
//
//    const unsigned int size = 64;
//    size_t cubeDataSize = size * size * size * sizeof ( float ) * 4;
//    float *cubeData = (float *) malloc ( cubeDataSize );
//    float rgb[3], hsv[3], newRGB[3];
//
//    size_t offset = 0;
//
//    for (int z = 0; z < size; z++)
//    {
//        rgb[2] = ((double) z) / size; // blue value
//        for (int y = 0; y < size; y++)
//        {
//            rgb[1] = ((double) y) / size; // green value
//            for (int x = 0; x < size; x++)
//            {
//                rgb[0] = ((double) x) / size; // red value
//                rgbToHSV(rgb, hsv);
//
//                if (hsv[0] < minHueAngle || hsv[0] > maxHueAngle)
//                    memcpy(newRGB, rgb, sizeof(newRGB));
//                else
//                {
//                    hsv[0] = destCenterHueAngle + (centerHueAngle - hsv[0]);
//                    hsvToRGB(hsv, newRGB);
//                }
//
//                cubeData[offset]   = newRGB[0];
//                cubeData[offset+1] = newRGB[1];
//                cubeData[offset+2] = newRGB[2];
//                cubeData[offset+3] = 1.0;
//
//                offset += 4;
//            }
//        }
//    }
//
//    NSData *data = [NSData dataWithBytesNoCopy:cubeData length:cubeDataSize freeWhenDone:YES];
//
//    return data;
//}

@end


@interface SummerLinearFilter : SummerEmptyFilter
@end
@implementation SummerLinearFilter
@end

@interface Summer1 : SummerEmptyFilter
@end
@implementation Summer1
@end

@interface Summer2 : SummerEmptyFilter
@end
@implementation Summer2
@end

@interface Summer3 : SummerEmptyFilter
@end
@implementation Summer3
@end

@interface Summer4 : SummerEmptyFilter
@end
@implementation Summer4
@end

@interface Summer5 : SummerEmptyFilter
@end
@implementation Summer5
@end

@interface Summer6 : SummerEmptyFilter
@end
@implementation Summer6
@end

