//
//  BasicFilterBase.h
//  TestEditor
//
//  Created by Md.Ballal Hossen on 24/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ImageToolSettings.h"

@protocol BasicFilterBaseProtocol <NSObject>

@required
+ (UIImage*)applyFilter:(UIImage*)image;

@end
@interface BasicFilterBase : NSObject<
ImageToolProtocol,
BasicFilterBaseProtocol
>

@end

