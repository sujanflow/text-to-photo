//
//  DuoToneFliterBase.h
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 26/5/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ImageToolSettings.h"


@protocol DuoToneFilterBaseProtocol <NSObject>

@required
+ (UIImage*)applyFilter:(UIImage*)image;

@end
@interface DuoToneFilterBase : NSObject<
ImageToolProtocol,
DuoToneFilterBaseProtocol
>

@end
