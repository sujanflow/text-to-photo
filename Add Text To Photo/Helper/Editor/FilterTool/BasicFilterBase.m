//
//  BasicFilterBase.m
//  TestEditor
//
//  Created by Md.Ballal Hossen on 24/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "BasicFilterBase.h"

@implementation BasicFilterBase


+ (NSString*)defaultIconImagePath
{
    return nil;
}

+ (NSArray*)subtools
{
    return nil;
}


+ (CGFloat)defaultDockedNumber
{
    return 0;
}


+ (NSString*)defaultTitle
{
    return @"BasicFilterBase";
}

+ (BOOL)isAvailable
{
    return NO;
}

+ (NSDictionary*)optionalInfo
{
    return nil;
}

#pragma mark-

+ (UIImage*)applyFilter:(UIImage*)image
{
    return image;
}

#pragma mark- FILTERS LIST ============================

@end

@interface BasicEmptyFilter : BasicFilterBase

@end

@implementation BasicEmptyFilter

+ (NSDictionary*)defaultFilterInfo
{
    NSDictionary *defaultFilterInfo = nil;
    if (defaultFilterInfo == nil){
        defaultFilterInfo =
        @{
          @"BasicEmptyFilter": @{@"name":@"BasicEmptyFilter",
                            @"title":NSLocalizedString(@"BasicEmptyFilter", @""),
                            @"version":@(0.0), @"dockedNum":@(0.0) },
          
          @"BasicLinearFilter": @{@"name":@"CISRGBToneCurveToLinear",
                             @"title":NSLocalizedString(@"LinearFilter", @""),
                             @"version":@(7.0), @"dockedNum":@(1.0)},
          
          @"BasicVignetteFilter": @{@"name":@"CIVignetteEffect",
                               @"title":NSLocalizedString(@"VignetteFilter", @""),
                               @"version":@(7.0), @"dockedNum":@(2.0)},
          //Tone Curve
          @"BasicToneCurve": @{@"name":@"CIToneCurve",
                                    @"title":NSLocalizedString(@"ToneCurve", @""),
                                    @"version":@(7.0), @"dockedNum":@(3.0)}

          
          };
    }
    return defaultFilterInfo;
}

+ (id)defaultInfoForKey:(NSString*)key {
    return self.defaultFilterInfo[NSStringFromClass(self)][key];
}

+ (NSString*)filterName {
    return [self defaultInfoForKey:@"name"];
}

+ (NSString*)defaultTitle {
    return [self defaultInfoForKey:@"title"];
}

+ (BOOL)isAvailable {
    return true;
    //([UIDevice iosVersion] >= [[self defaultInfoForKey:@"version"] floatValue]);
}

+ (CGFloat)defaultDockedNumber {
    return [[self defaultInfoForKey:@"dockedNum"] floatValue];
}

#pragma mark - FILTERS IMPLEMENTATION ======================================

+ (UIImage*)applyFilter:(UIImage *)image
{
    return [self filteredImage:image withFilterName:self.filterName];
}

+ (UIImage*)filteredImage:(UIImage*)image withFilterName:(NSString*)filterName {
    
    // No Filter ========================
    if([filterName isEqualToString:@"BasicEmptyFilter"]){
        return image;
    }
    
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
    
    // NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    
    // CIVignette Filter ===============
    if([filterName isEqualToString:@"CIVignetteEffect"]){
        // parameters
        CGFloat R = MIN(image.size.width, image.size.height)/2;
        CIVector *vct = [[CIVector alloc] initWithX:image.size.width/2 Y:image.size.height/2];
        [filter setValue:vct forKey:@"inputCenter"];
        [filter setValue:[NSNumber numberWithFloat:0.9] forKey:@"inputIntensity"];
        [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
    }
    if([filterName isEqualToString:@"CIToneCurve"]){
        
        NSLog(@"CIToneCurve???????????????");
        
        [filter setValue:[CIVector vectorWithX:0.0  Y:0.0] forKey:@"inputPoint0"]; // default
        [filter setValue:[CIVector vectorWithX:0.27 Y:0.26] forKey:@"inputPoint1"];
        [filter setValue:[CIVector vectorWithX:0.5  Y:0.80] forKey:@"inputPoint2"];
        [filter setValue:[CIVector vectorWithX:0.7  Y:1.0] forKey:@"inputPoint3"];
        [filter setValue:[CIVector vectorWithX:1.0  Y:1.0] forKey:@"inputPoint4"]; // default

    }
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    return result;
}

@end
    

@interface BasicLinearFilter : BasicEmptyFilter
@end
@implementation BasicLinearFilter
@end

@interface BasicVignetteFilter : BasicEmptyFilter
@end
@implementation BasicVignetteFilter
@end

@interface BasicToneCurve : BasicEmptyFilter
@end
@implementation BasicToneCurve
@end
