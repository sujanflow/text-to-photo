//
//  VintageFliterBase.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 26/5/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "VintageFilterBase.h"

#import <UIKit/UIKit.h>


@implementation VintageFilterBase



+ (NSString*)defaultIconImagePath
{
    return nil;
}

+ (NSArray*)subtools
{
    return nil;
}


+ (CGFloat)defaultDockedNumber
{
    return 0;
}


+ (NSString*)defaultTitle
{
    return @"VintageFilterBase";
}

+ (BOOL)isAvailable
{
    return NO;
}

+ (NSDictionary*)optionalInfo
{
    return nil;
}

#pragma mark-

+ (UIImage*)applyFilter:(UIImage*)image
{
    return image;
}

#pragma mark- FILTERS LIST ============================

@end


@interface VintageEmptyFilter : VintageFilterBase

@end

@implementation VintageEmptyFilter

+ (NSDictionary*)defaultFilterInfo
{
    NSDictionary *defaultFilterInfo = nil;
    if (defaultFilterInfo == nil){
        defaultFilterInfo =
        @{
          @"VintageEmptyFilter": @{@"name":@"VintageEmptyFilter",
                                   @"title":NSLocalizedString(@"VintageEmptyFilter", @""),
                                   @"version":@(0.0), @"dockedNum":@(0.0) },
          
          @"Vintage1": @{@"name":@"CIVignette",
                         @"title":NSLocalizedString(@"Vintage1", @""),
                         @"version":@(7.0), @"dockedNum":@(1.0)},
          
          @"Vintage2": @{@"name":@"CIVignette",
                         @"title":NSLocalizedString(@"Vintage2", @""),
                         @"version":@(7.0), @"dockedNum":@(2.0)},
          
          @"Vintage3": @{@"name":@"CIVignette",
                         @"title":NSLocalizedString(@"Vintage3", @""),
                         @"version":@(7.0), @"dockedNum":@(3.0)},
          @"Vintage4": @{@"name":@"CIVignette",
                         @"title":NSLocalizedString(@"Vintage4", @""),
                         @"version":@(7.0), @"dockedNum":@(4.0)},
          @"Vintage5": @{@"name":@"CIVignette",
                         @"title":NSLocalizedString(@"Vintage5", @""),
                         @"version":@(7.0), @"dockedNum":@(5.0)}


          
          
          };
    }
    return defaultFilterInfo;
}

+ (id)defaultInfoForKey:(NSString*)key {
    return self.defaultFilterInfo[NSStringFromClass(self)][key];
}

+ (NSString*)filterName {
    return [self defaultInfoForKey:@"name"];
}

+ (NSString*)defaultTitle {
    return [self defaultInfoForKey:@"title"];
}

+ (BOOL)isAvailable {
    return true;
    //([UIDevice iosVersion] >= [[self defaultInfoForKey:@"version"] floatValue]);
}

+ (CGFloat)defaultDockedNumber {
    return [[self defaultInfoForKey:@"dockedNum"] floatValue];
}

#pragma mark - FILTERS IMPLEMENTATION ======================================

+ (UIImage*)applyFilter:(UIImage *)image
{
    return [self title:self.defaultTitle  filteredImage:image withFilterName:self.filterName];
}

+ (UIImage*)title:(NSString*)title filteredImage:(UIImage*)image withFilterName:(NSString*)filterName {
    
    // No Filter ========================
    if([filterName isEqualToString:@"VintageEmptyFilter"]){
        return image;
    }
    
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
    
    // NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    
    if([title isEqualToString:@"Vintage1"]){
        
        NSLog(@"Vintage1");
        CIImage *output1 = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey, ciImage, nil].outputImage;
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIVignette" keysAndValues:kCIInputImageKey, output1, @"inputRadius", [NSNumber numberWithFloat:2], @"inputIntensity", [NSNumber numberWithFloat:1], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
        
        
    }else if ([title isEqualToString:@"Vintage2"]){
        
        CIImage *output1 = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey, ciImage, nil].outputImage;
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIVignette" keysAndValues:kCIInputImageKey, output1, @"inputRadius", [NSNumber numberWithFloat:1.5], @"inputIntensity", [NSNumber numberWithFloat:1], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
        
    }else if ([title isEqualToString:@"Vintage3"]){
        
        CIImage *output1 = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey, ciImage, nil].outputImage;
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIVignette" keysAndValues:kCIInputImageKey, output1, @"inputRadius", [NSNumber numberWithFloat:1], @"inputIntensity", [NSNumber numberWithFloat:1], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;

    }else if ([title isEqualToString:@"Vintage4"]){
        
        CIImage *output1 = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey, ciImage, nil].outputImage;
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIVignette" keysAndValues:kCIInputImageKey, output1, @"inputRadius", [NSNumber numberWithFloat:0.5], @"inputIntensity", [NSNumber numberWithFloat:2], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
        
    }else if ([title isEqualToString:@"Vintage5"]){
        
        CIImage *output1 = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey, ciImage, nil].outputImage;

        CIContext *context = [CIContext contextWithOptions:nil];

        CIImage *output = [CIFilter filterWithName:@"CIVignette" keysAndValues:kCIInputImageKey, output1, @"inputRadius", [NSNumber numberWithFloat:2.5], @"inputIntensity", [NSNumber numberWithFloat:2], nil].outputImage;

        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];

        UIImage *result = [UIImage imageWithCGImage:cgImage];

        CGImageRelease(cgImage);
        return result;
       

    }
    
    return nil;
    
}

@end


@interface VintageLinearFilter : VintageEmptyFilter
@end
@implementation VintageLinearFilter
@end

@interface Vintage1 : VintageEmptyFilter
@end
@implementation Vintage1
@end

@interface Vintage2 : VintageEmptyFilter
@end
@implementation Vintage2
@end

@interface Vintage3 : VintageEmptyFilter
@end
@implementation Vintage3
@end

@interface Vintage4 : VintageEmptyFilter
@end
@implementation Vintage4
@end

@interface Vintage5 : VintageEmptyFilter
@end
@implementation Vintage5
@end
