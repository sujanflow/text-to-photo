//
//  B&WFilterBase.h
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 17/5/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageToolSettings.h"

NS_ASSUME_NONNULL_BEGIN



@protocol BWFilterBaseProtocol <NSObject>

@required
+ (UIImage*)applyFilter:(UIImage*)image;

@end


@interface B_WFilterBase : NSObject<
ImageToolProtocol,
BWFilterBaseProtocol
>

@end

NS_ASSUME_NONNULL_END
