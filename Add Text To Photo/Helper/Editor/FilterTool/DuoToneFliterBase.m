//
//  DuoToneFliterBase.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 26/5/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "DuoToneFliterBase.h"
#import <UIKit/UIKit.h>


@implementation DuoToneFilterBase



+ (NSString*)defaultIconImagePath
{
    return nil;
}

+ (NSArray*)subtools
{
    return nil;
}


+ (CGFloat)defaultDockedNumber
{
    return 0;
}


+ (NSString*)defaultTitle
{
    return @"DuoToneFilterBase";
}

+ (BOOL)isAvailable
{
    return NO;
}

+ (NSDictionary*)optionalInfo
{
    return nil;
}

#pragma mark-

+ (UIImage*)applyFilter:(UIImage*)image
{
    return image;
}

#pragma mark- FILTERS LIST ============================

@end


@interface DuoToneEmptyFilter : DuoToneFilterBase

@end

@implementation DuoToneEmptyFilter

+ (NSDictionary*)defaultFilterInfo
{
    NSDictionary *defaultFilterInfo = nil;
    if (defaultFilterInfo == nil){
        defaultFilterInfo =
        @{
          @"DuoToneEmptyFilter": @{@"name":@"DuoToneEmptyFilter",
                                  @"title":NSLocalizedString(@"DuoToneEmptyFilter", @""),
                                  @"version":@(0.0), @"dockedNum":@(0.0) },
          
          @"DuoTone1": @{@"name":@"CIColorMonochrome",
                        @"title":NSLocalizedString(@"DuoTone1", @""),
                        @"version":@(7.0), @"dockedNum":@(1.0)},
          
          @"DuoTone2": @{@"name":@"CIColorMonochrome",
                        @"title":NSLocalizedString(@"DuoTone2", @""),
                        @"version":@(7.0), @"dockedNum":@(2.0)},
          
          @"DuoTone3": @{@"name":@"CIColorMonochrome",
                        @"title":NSLocalizedString(@"DuoTone3", @""),
                        @"version":@(7.0), @"dockedNum":@(3.0)},
          @"DuoTone4": @{@"name":@"CIColorMonochrome",
                         @"title":NSLocalizedString(@"DuoTone4", @""),
                         @"version":@(7.0), @"dockedNum":@(4.0)},
          @"DuoTone5": @{@"name":@"CIColorMonochrome",
                         @"title":NSLocalizedString(@"DuoTone5", @""),
                         @"version":@(7.0), @"dockedNum":@(5.0)},
          @"DuoTone6": @{@"name":@"CIColorMonochrome",
                         @"title":NSLocalizedString(@"DuoTone6", @""),
                         @"version":@(7.0), @"dockedNum":@(6.0)},
          @"DuoTone7": @{@"name":@"CIColorMonochrome",
                         @"title":NSLocalizedString(@"DuoTone7", @""),
                         @"version":@(7.0), @"dockedNum":@(7.0)},
          @"DuoTone8": @{@"name":@"CIColorMonochrome",
                         @"title":NSLocalizedString(@"DuoTone8", @""),
                         @"version":@(7.0), @"dockedNum":@(8.0)},
          @"DuoTone9": @{@"name":@"CIColorMonochrome",
                         @"title":NSLocalizedString(@"DuoTone9", @""),
                         @"version":@(7.0), @"dockedNum":@(9.0)}


          
          
          };
    }
    return defaultFilterInfo;
}

+ (id)defaultInfoForKey:(NSString*)key {
    return self.defaultFilterInfo[NSStringFromClass(self)][key];
}

+ (NSString*)filterName {
    return [self defaultInfoForKey:@"name"];
}

+ (NSString*)defaultTitle {
    return [self defaultInfoForKey:@"title"];
}

+ (BOOL)isAvailable {
    return true;
    //([UIDevice iosVersion] >= [[self defaultInfoForKey:@"version"] floatValue]);
}

+ (CGFloat)defaultDockedNumber {
    return [[self defaultInfoForKey:@"dockedNum"] floatValue];
}

#pragma mark - FILTERS IMPLEMENTATION ======================================

+ (UIImage*)applyFilter:(UIImage *)image
{
    return [self title:self.defaultTitle  filteredImage:image withFilterName:self.filterName];
}

+ (UIImage*)title:(NSString*)title filteredImage:(UIImage*)image withFilterName:(NSString*)filterName {
    
    // No Filter ========================
    if([filterName isEqualToString:@"DuoToneEmptyFilter"]){
        return image;
    }
    
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
    
    // NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    
    if([title isEqualToString:@"DuoTone1"]){
        
        NSLog(@"DuoTone1");
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues:kCIInputImageKey, ciImage, @"inputIntensity", [NSNumber numberWithFloat:0.5], @"inputColor", [[CIColor alloc] initWithColor:[UIColor redColor]], nil].outputImage;

        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;

        
    }else if ([title isEqualToString:@"DuoTone2"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues:kCIInputImageKey, ciImage, @"inputIntensity", [NSNumber numberWithFloat:0.5], @"inputColor", [[CIColor alloc] initWithColor:[UIColor greenColor]], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;

        
    }else if ([title isEqualToString:@"DuoTone3"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues:kCIInputImageKey, ciImage, @"inputIntensity", [NSNumber numberWithFloat:0.5], @"inputColor", [[CIColor alloc] initWithColor:[UIColor blueColor]], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;

        
    }else if ([title isEqualToString:@"DuoTone4"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues:kCIInputImageKey, ciImage, @"inputIntensity", [NSNumber numberWithFloat:0.5], @"inputColor", [[CIColor alloc] initWithColor:[UIColor yellowColor]], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
        
        
    }else if ([title isEqualToString:@"DuoTone5"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues:kCIInputImageKey, ciImage, @"inputIntensity", [NSNumber numberWithFloat:0.5], @"inputColor", [[CIColor alloc] initWithColor:[UIColor cyanColor]], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
        
        
    }else if ([title isEqualToString:@"DuoTone6"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues:kCIInputImageKey, ciImage, @"inputIntensity", [NSNumber numberWithFloat:0.5], @"inputColor", [[CIColor alloc] initWithColor:[UIColor magentaColor]], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
        
        
    }else if ([title isEqualToString:@"DuoTone7"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues:kCIInputImageKey, ciImage, @"inputIntensity", [NSNumber numberWithFloat:0.5], @"inputColor", [[CIColor alloc] initWithColor:[UIColor orangeColor]], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
        
        
    }else if ([title isEqualToString:@"DuoTone8"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues:kCIInputImageKey, ciImage, @"inputIntensity", [NSNumber numberWithFloat:0.7], @"inputColor", [[CIColor alloc] initWithColor:[UIColor purpleColor]], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
        
        
    }else if ([title isEqualToString:@"DuoTone9"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        
        CIImage *output = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues:kCIInputImageKey, ciImage, @"inputIntensity", [NSNumber numberWithFloat:0.7], @"inputColor", [[CIColor alloc] initWithColor:[UIColor brownColor]], nil].outputImage;
        
        CGImageRef cgImage = [context createCGImage:output fromRect:[output extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        return result;
        
        
    }
    
    return nil;
    
}

@end


@interface DuoToneLinearFilter : DuoToneEmptyFilter
@end
@implementation DuoToneLinearFilter
@end

@interface DuoTone1 : DuoToneEmptyFilter
@end
@implementation DuoTone1
@end

@interface DuoTone2 : DuoToneEmptyFilter
@end
@implementation DuoTone2
@end

@interface DuoTone3 : DuoToneEmptyFilter
@end
@implementation DuoTone3
@end

@interface DuoTone4 : DuoToneEmptyFilter
@end
@implementation DuoTone4
@end

@interface DuoTone5 : DuoToneEmptyFilter
@end
@implementation DuoTone5
@end

@interface DuoTone6 : DuoToneEmptyFilter
@end
@implementation DuoTone6
@end

@interface DuoTone7 : DuoToneEmptyFilter
@end
@implementation DuoTone7
@end

@interface DuoTone8 : DuoToneEmptyFilter
@end
@implementation DuoTone8
@end

@interface DuoTone9 : DuoToneEmptyFilter
@end
@implementation DuoTone9
@end

