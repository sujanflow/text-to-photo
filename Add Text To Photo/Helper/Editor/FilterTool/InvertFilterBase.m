//
//  InvertFilterBase.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 31/5/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "InvertFilterBase.h"

#import <UIKit/UIKit.h>


@implementation InvertFilterBase



+ (NSString*)defaultIconImagePath
{
    return nil;
}

+ (NSArray*)subtools
{
    return nil;
}


+ (CGFloat)defaultDockedNumber
{
    return 0;
}


+ (NSString*)defaultTitle
{
    return @"InvertFilterBase";
}

+ (BOOL)isAvailable
{
    return NO;
}

+ (NSDictionary*)optionalInfo
{
    return nil;
}

#pragma mark-

+ (UIImage*)applyFilter:(UIImage*)image
{
    return image;
}

#pragma mark- FILTERS LIST ============================

@end


@interface InvertEmptyFilter : InvertFilterBase

@end

@implementation InvertEmptyFilter

+ (NSDictionary*)defaultFilterInfo
{
    NSDictionary *defaultFilterInfo = nil;
    if (defaultFilterInfo == nil){
        defaultFilterInfo =
        @{
          @"InvertEmptyFilter": @{@"name":@"InvertEmptyFilter",
                                 @"title":NSLocalizedString(@"InvertEmptyFilter", @""),
                                 @"version":@(0.0), @"dockedNum":@(0.0) },
          
          @"Invert1": @{@"name":@"CISepiaTone",
                       @"title":NSLocalizedString(@"Invert1", @""),
                       @"version":@(7.0), @"dockedNum":@(1.0)},
          
          @"Invert2": @{@"name":@"CISepiaTone",
                       @"title":NSLocalizedString(@"Invert2", @""),
                       @"version":@(7.0), @"dockedNum":@(2.0)},
          
          @"Invert3": @{@"name":@"CISepiaTone",
                       @"title":NSLocalizedString(@"Invert3", @""),
                       @"version":@(7.0), @"dockedNum":@(3.0)},
          @"Invert4": @{@"name":@"CISepiaTone",
                       @"title":NSLocalizedString(@"Invert4", @""),
                       @"version":@(7.0), @"dockedNum":@(4.0)},
          
          @"Invert5": @{@"name":@"CISepiaTone",
                       @"title":NSLocalizedString(@"Invert5", @""),
                       @"version":@(7.0), @"dockedNum":@(5.0)}
          
          
          
          };
    }
    return defaultFilterInfo;
}

+ (id)defaultInfoForKey:(NSString*)key {
    return self.defaultFilterInfo[NSStringFromClass(self)][key];
}

+ (NSString*)filterName {
    return [self defaultInfoForKey:@"name"];
}

+ (NSString*)defaultTitle {
    return [self defaultInfoForKey:@"title"];
}

+ (BOOL)isAvailable {
    return true;
    //([UIDevice iosVersion] >= [[self defaultInfoForKey:@"version"] floatValue]);
}

+ (CGFloat)defaultDockedNumber {
    return [[self defaultInfoForKey:@"dockedNum"] floatValue];
}

#pragma mark - FILTERS IMPLEMENTATION ======================================

+ (UIImage*)applyFilter:(UIImage *)image
{
    return [self title:self.defaultTitle  filteredImage:image withFilterName:self.filterName];
}

+ (UIImage*)title:(NSString*)title filteredImage:(UIImage*)image withFilterName:(NSString*)filterName {
    
    // No Filter ========================
    if([filterName isEqualToString:@"InvertEmptyFilter"]){
        return image;
    }
    
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
    
    // NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    
    if([title isEqualToString:@"Invert1"]){
        
        NSLog(@"Invert1");
        CIFilter *clFilter = [CIFilter filterWithName:@"CIColorCrossPolynomial"];
        
        
        
        CIVector *vct = [[CIVector alloc] initWithString:@"[0 1 0 0 0 0 0 0 0 0]"];
        CIVector *vct1 = [[CIVector alloc] initWithString:@"[1 0 0 0 0 0 0 0 0 0]"];
        CIVector *vct2 = [[CIVector alloc] initWithString:@"[1 0 0 0 0 0 0 0 0 0]"];

        [clFilter setValue:ciImage forKey:@"inputImage"];
        [clFilter setValue:vct forKey:@"inputRedCoefficients"];
        [clFilter setValue:vct1 forKey:@"inputGreenCoefficients"];
        [clFilter setValue:vct2 forKey:@"inputBlueCoefficients"];

        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [clFilter outputImage];
        
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        
        return result;

        
    }else if ([title isEqualToString:@"Invert2"]){
        
        CIFilter *clFilter = [CIFilter filterWithName:@"CIColorCrossPolynomial"];
        
        
        
        CIVector *vct = [[CIVector alloc] initWithString:@"[1 0 0 0 0 0 0 0 1 0]"];
        CIVector *vct1 = [[CIVector alloc] initWithString:@"[0 0 0 0 0 1 0 0 0 0]"];
        CIVector *vct2 = [[CIVector alloc] initWithString:@"[1 0 0 1 0 0 1 0 0 0]"];
        
        [clFilter setValue:ciImage forKey:@"inputImage"];
        [clFilter setValue:vct forKey:@"inputRedCoefficients"];
        [clFilter setValue:vct1 forKey:@"inputGreenCoefficients"];
        [clFilter setValue:vct2 forKey:@"inputBlueCoefficients"];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [clFilter outputImage];
        
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        
        return result;

        
    }else if ([title isEqualToString:@"Invert3"]){
        
        CIFilter *clFilter = [CIFilter filterWithName:@"CIColorCrossPolynomial"];
        
        
        
        CIVector *vct = [[CIVector alloc] initWithString:@"[1 0 0 0 0 0 0 0 0 0]"];
        CIVector *vct1 = [[CIVector alloc] initWithString:@"[1 0 0 0 0 0 0 1 0 0]"];
        CIVector *vct2 = [[CIVector alloc] initWithString:@"[1 0 0 1 0 0 0 0 0 0]"];
        
        [clFilter setValue:ciImage forKey:@"inputImage"];
        [clFilter setValue:vct forKey:@"inputRedCoefficients"];
        [clFilter setValue:vct1 forKey:@"inputGreenCoefficients"];
        [clFilter setValue:vct2 forKey:@"inputBlueCoefficients"];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [clFilter outputImage];
        
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        
        return result;

    }else if ([title isEqualToString:@"Invert4"]){
        
        CIFilter *clFilter = [CIFilter filterWithName:@"CIColorCrossPolynomial"];
        
        
        
        CIVector *vct = [[CIVector alloc] initWithString:@"[1 0 0 0 1 0 0 0 0 0]"];
        CIVector *vct1 = [[CIVector alloc] initWithString:@"[1 0 0 1 0 0 0 0 0 0]"];
        CIVector *vct2 = [[CIVector alloc] initWithString:@"[1 0 0 0 0 0 0 0 0 0]"];
        
        [clFilter setValue:ciImage forKey:@"inputImage"];
        [clFilter setValue:vct forKey:@"inputRedCoefficients"];
        [clFilter setValue:vct1 forKey:@"inputGreenCoefficients"];
        [clFilter setValue:vct2 forKey:@"inputBlueCoefficients"];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [clFilter outputImage];
        
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        
        return result;

    }else if ([title isEqualToString:@"Invert5"]){
        
        CIFilter *clFilter = [CIFilter filterWithName:@"CIColorCrossPolynomial"];
        
        CIVector *vct = [[CIVector alloc] initWithString:@"[0 1 0 0 0 0 0 0 0 0]"];
        CIVector *vct1 = [[CIVector alloc] initWithString:@"[0 0 0 0 0 0 0 1 0 0]"];
        CIVector *vct2 = [[CIVector alloc] initWithString:@"[0 0 0 0 0 0 0 0 0 0]"];
        
        [clFilter setValue:ciImage forKey:@"inputImage"];
        [clFilter setValue:vct forKey:@"inputRedCoefficients"];
        [clFilter setValue:vct1 forKey:@"inputGreenCoefficients"];
        [clFilter setValue:vct2 forKey:@"inputBlueCoefficients"];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [clFilter outputImage];
        
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        CGImageRelease(cgImage);
        
        return result;

    }
    
    return nil;
    
}

@end


@interface InvertLinearFilter : InvertEmptyFilter
@end
@implementation InvertLinearFilter
@end

@interface Invert1 : InvertEmptyFilter
@end
@implementation Invert1
@end

@interface Invert2 : InvertEmptyFilter
@end
@implementation Invert2
@end

@interface Invert3 : InvertEmptyFilter
@end
@implementation Invert3
@end

@interface Invert4 : InvertEmptyFilter
@end
@implementation Invert4
@end

@interface Invert5 : InvertEmptyFilter
@end
@implementation Invert5
@end
