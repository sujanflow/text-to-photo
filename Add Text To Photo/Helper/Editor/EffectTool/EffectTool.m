/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import "EffectTool.h"
#import "Configs.h"

//#import "IAPController.h"


@interface EffectTool()
@property (nonatomic, strong) UIView *selectedMenu;
@property (nonatomic, strong) EffectBase *selectedEffect;
@end


@implementation EffectTool
{
    UIImage *_originalImage;
    UIImage *_thumnailImage;
    
    UIScrollView *_menuScroll;
    UIActivityIndicatorView *_indicatorView;
    
    // For IAP =========
    UIImageView *iapDot;
   // NSTimer *iapTimer;
    int tagINT,
    effectsTAG;
    
    BOOL iapMade;
    BOOL iapForEffects;
}

+ (NSArray*)subtools
{
    return [ImageToolInfo toolsWithToolClass:[EffectBase class]];
}

+ (NSString*)defaultTitle
{
    return NSLocalizedString(@"Effect", @"");
}

+ (BOOL)isAvailable {
    return false;
}



#pragma mark- SETUP ===============

- (void)setup {
    NSLog(@"setup");
    _originalImage = self.editor.imageView.image;
    _thumnailImage = _originalImage;
    
    // Fire IAP timer
//    iapTimer = [NSTimer scheduledTimerWithTimeInterval:0.2  target:self selector:@selector(removeIapDots:)  userInfo:nil repeats:true];
    tagINT = 0;
    effectsTAG = -1;
    
    _menuScroll = [[UIScrollView alloc] initWithFrame:self.editor.menuView.frame];
    _menuScroll.backgroundColor = self.editor.menuView.backgroundColor;
    _menuScroll.showsHorizontalScrollIndicator = NO;
    [self.editor.view addSubview:_menuScroll];
    
    // Fire IAP timer
    
    tagINT = 0;
    
    [self setEffectsMenu];
    
    _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height-_menuScroll.top);
    [UIView animateWithDuration:kImageToolAnimationDuration animations:^{
        _menuScroll.transform = CGAffineTransformIdentity;
    }];
}

- (void)cleanup {
    NSLog(@"cleanup");
   //[iapTimer invalidate];
    
    [self.selectedEffect cleanup];
    [_indicatorView removeFromSuperview];
    
    [self.editor resetZoomScaleWithAnimated: true];
    
    [UIView animateWithDuration:kImageToolAnimationDuration animations:^{
        _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height-_menuScroll.top);
    } completion:^(BOOL finished) {
        [_menuScroll removeFromSuperview];
    }];
}

#pragma mark - REMOVE IAP DOTS METHOD  =================
// Remove iapDots icons from items that have been purchased with IAP

-(void)refreshViewAfterInApp
{
    NSLog(@"refreshViewAfterInApp");
    [self removeIapDots];
    
//    if(isAdRemoved)
//        _menuScroll.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height-55 , [UIScreen mainScreen].bounds.size.width , 55);
    
}
-(void)removeIapDots  {
//    if (iapForEffects || iapMade) {
//        for (int i = 400+freeEffects; i <= iapDot.tag; i++) {
//            [[self.editor.view viewWithTag:i] removeFromSuperview];
//        }
       // [iapTimer invalidate];
 //   }
    // NSLog(@"timerON!");
}

- (void)executeWithCompletionBlock:(void(^)(UIImage *image, NSError *error, NSDictionary *userInfo))completionBlock
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _indicatorView = [ImageEditorTheme indicatorView];
        _indicatorView.center = self.editor.view.center;
        [self.editor.view addSubview:_indicatorView];
        [_indicatorView startAnimating];
    });
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image = [self.selectedEffect applyEffect:_originalImage];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock(image, nil, nil);
        });
    });
}



#pragma mark - SETUP EFFECTS MENU =====================

- (void)setEffectsMenu {
    CGFloat W = 70;
    CGFloat H = _menuScroll.height;
    CGFloat x = 0;
    
    for(ImageToolInfo *info in self.toolInfo.sortedSubtools){
        if(!info.available){
            continue;
        }
        
        ToolbarMenuItem *item = [ImageEditorTheme menuItemWithFrame:CGRectMake(x, 0, W, H) target:self action:@selector(tappedMenu:) toolInfo:info];
        
        tagINT++;
        item.tag = tagINT;
        
        // Add a little circle on the top of the PAID items that need to be unlocked with IAP
        if (!iapForEffects && !iapMade && item.tag >= freeEffects) {
            iapDot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 6, 6)];
            iapDot.backgroundColor = PURPLE_COLOR;
            iapDot.layer.cornerRadius = iapDot.bounds.size.width/2;
            iapDot.tag = tagINT+600;
            [item addSubview:iapDot];
            //NSLog(@"iapDot TAG: %ld", (long)iapDot.tag);
        }
        //====================================================================
        [_menuScroll addSubview:item];
        
        x += W;
        
        if(item.selected==nil){
            item.selected = Nil;
        }
    }
    _menuScroll.contentSize = CGSizeMake(MAX(x, _menuScroll.frame.size.width+1), 0);
}

- (void)tappedMenu:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
    view.alpha = 0.2;
    [UIView animateWithDuration:kImageToolAnimationDuration animations:^{
        view.alpha = 1;
    }];
    
    if (!iapForEffects && !iapMade && view.tag >= freeFilters) {
//        IAPController *iapVC = [[IAPController alloc]initWithNibName:@"IAPController" bundle:nil];
//        iapVC.delegate=self.editor;
//        [self.editor presentViewController: iapVC animated:true completion:nil];
//        
        /*========================================================================================
         IAP MADE!
         =========================================================================================*/
    } else {
       
        self.selectedMenu = view;
        
        effectsTAG++;
        view.tag = effectsTAG;
    }
    

    
//    else if(view.tag<=3){
//    
//        view.alpha = 0.2;
//        [UIView animateWithDuration:kImageToolAnimationDuration animations:^{
//        view.alpha = 1;
//        }];
//    
//        self.selectedMenu = view;
//    }
//    
   
}




- (void)setSelectedMenu:(UIView *)selectedMenu {
    if(selectedMenu != _selectedMenu){
        _selectedMenu.backgroundColor = [UIColor clearColor];
        _selectedMenu = selectedMenu;
        
        Class effectClass = NSClassFromString(_selectedMenu.toolInfo.toolName);
        self.selectedEffect = [[effectClass alloc] initWithSuperView:self.editor.imageView.superview imageViewFrame:self.editor.imageView.frame toolInfo:_selectedMenu.toolInfo];
    }
}

- (void)setSelectedEffect:(EffectBase *)selectedEffect
{
    if(selectedEffect != _selectedEffect){
        [_selectedEffect cleanup];
        _selectedEffect = selectedEffect;
        _selectedEffect.delegate = self;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self buildThumnailImage];
        });
    }
}

- (void)buildThumnailImage
{
    UIImage *image;
    if(self.selectedEffect.needsThumnailPreview){
        image = [self.selectedEffect applyEffect:_thumnailImage];
    }
    else{
        image = [self.selectedEffect applyEffect:_originalImage];
    }
    [self.editor.imageView performSelectorOnMainThread:@selector(setImage:) withObject:image waitUntilDone:NO];
}

#pragma mark- Effect delegate

- (void)effectParameterDidChange:(EffectBase *)effect
{
    if(effect == self.selectedEffect){
        static BOOL inProgress = NO;
        
        if(inProgress){ return; }
        inProgress = YES;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self buildThumnailImage];
            inProgress = NO;
        });
    }
}

//#pragma mark- Appirater delegate
//
//-(void)appiraterDidDeclineToRate:(Appirater *)appirater
//{
//    NSLog(@"appiraterDidDeclineToRate %@",_thumnailImage);
//
//    //[self cleanup];
//    [_selectedEffect cleanup];
//    [self.editor.imageView setImage:_originalImage];
//
//}
//
//-(void)appiraterDidOptToRate:(Appirater *)appirater
//{
//
//}

@end
