/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/



#import "UIDevice+SystemVersion.h"
#import "UIView+Frame.h"
#import "UIImage+Utility.h"

#import "ImageToolProtocol.h"
#import "ImageEditorTheme+Private.h"
#import "ImageToolInfo+Private.h"
