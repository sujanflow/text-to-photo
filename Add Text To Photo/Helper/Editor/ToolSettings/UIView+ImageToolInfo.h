/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/



#import <UIKit/UIKit.h>

#import "ImageToolInfo.h"

@interface UIView (ImageToolInfo)

@property (nonatomic, strong) ImageToolInfo *toolInfo;
@property (nonatomic, strong) NSDictionary *userInfo;

@end
