//
//  _CLImageEditorViewController.h
//
//  Created by sho yakushiji on 2013/11/05.
//  Copyright (c) 2013年 CALACULU. All rights reserved.
//

#import "ImageEditor.h"
#import "ISVImageScrollView.h"

@interface _CLImageEditorViewController : ImageEditor
<UIScrollViewDelegate, UIBarPositioningDelegate>
{
    IBOutlet __weak UINavigationBar *_navigationBar;
   // IBOutlet __weak UIScrollView *_scrollView;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightButtItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftButtItem;

@property (weak, nonatomic) IBOutlet UIView *navView;
@property (weak, nonatomic) IBOutlet UILabel *navTitleLabel;

@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;



@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *cancleButton;


@property (nonatomic, strong) UIImageView  *imageView;
@property (nonatomic, weak) IBOutlet UIScrollView *menuView;
//@property (nonatomic, readonly) ISVImageScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ISVImageScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *shareView;

@property (weak, nonatomic) IBOutlet UIView *rateUsBgView;

@property (weak, nonatomic) IBOutlet UIView *rateUsView;



- (IBAction)pushedCloseBtn:(id)sender;
- (IBAction)pushedFinishBtn:(id)sender;


- (id)initWithImage:(UIImage*)image;


- (void)fixZoomScaleWithAnimated:(BOOL)animated;
- (void)resetZoomScaleWithAnimated:(BOOL)animated;

@end
