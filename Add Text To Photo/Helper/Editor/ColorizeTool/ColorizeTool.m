/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import "ColorizeTool.h"
#import "ColorizeBase.h"

#import "Configs.h"


@interface ColorizeTool()

@end

@implementation ColorizeTool
{
    UIImage *_originalImage;
    
    UIScrollView *_menuScroll;
    
    // For IAP =========
    UIImageView *iapDot;
    NSTimer *iapTimer;
    int tagINT,
    stickersTAG;
}


+ (NSArray*)subtools
{
    return [ImageToolInfo toolsWithToolClass:[ColorizeBase class]];
}

+ (NSString*)defaultTitle
{
    return NSLocalizedString(@"ColorizeTool", @"");
}


+ (BOOL)isAvailable
{
    return NO;
}


- (void)setup
{
    _originalImage = self.editor.imageView.image;
    
    _menuScroll = [[UIScrollView alloc] initWithFrame:self.editor.menuView.frame];
    _menuScroll.backgroundColor = self.editor.menuView.backgroundColor;
    _menuScroll.showsHorizontalScrollIndicator = NO;
    [self.editor.view addSubview:_menuScroll];
    
    // Fire IAP timer
    
    tagINT = 0;
    
    [self setFilterMenu];
    
    _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height - _menuScroll.top);
   
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         _menuScroll.transform = CGAffineTransformIdentity;
                     }];
}

- (void)cleanup
{
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height-_menuScroll.top);
                     }
                     completion:^(BOOL finished) {
                         [_menuScroll removeFromSuperview];
                     }];
}

- (void)executeWithCompletionBlock:(void (^)(UIImage *, NSError *, NSDictionary *))completionBlock
{
    completionBlock(self.editor.imageView.image, nil, nil);
}

#pragma mark- 

- (void)setFilterMenu
{
    CGFloat W = 70;
    CGFloat x = 0;
    
    UIImage *iconThumnail = [_originalImage aspectFill:CGSizeMake(50, 50)];
    
    for(ImageToolInfo *info in self.toolInfo.sortedSubtools){
        if(!info.available) {
            continue;
        }
        
        ToolbarMenuItem *item = [ImageEditorTheme menuItemWithFrame:CGRectMake(x, 0, W, _menuScroll.height) target:self action:@selector(tappedFilterPanel:) toolInfo:info];
        [_menuScroll addSubview:item];
        tagINT++;
        item.tag = tagINT;
        x += W;
        
        if(item.iconImage==nil){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                UIImage *iconImage = [self filteredImage:iconThumnail withToolInfo:info];
                [item performSelectorOnMainThread:@selector(setIconImage:) withObject:iconImage waitUntilDone:NO];
            });
        }
    }
    _menuScroll.contentSize = CGSizeMake(MAX(x, _menuScroll.frame.size.width+1), 0);
}

- (void)tappedFilterPanel:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
    NSLog(@"tag: %ld", (long)view.tag);
    
    view.alpha = 0.2;
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         view.alpha = 1;
                     }
     ];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image = [self filteredImage:_originalImage withToolInfo:view.toolInfo];
        [self.editor.imageView performSelectorOnMainThread:@selector(setImage:) withObject:image waitUntilDone:NO];
    });
    
   // NSLog(@"view.toolInfo %@",view.toolInfo);
    
    
    if(view.tag>3)
    {
        int useCount=[[[NSUserDefaults standardUserDefaults] valueForKey:view.toolInfo.toolName] intValue];
        NSLog(@"useCount %d",useCount);
        if(useCount)
        {
            useCount++;
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:useCount] forKey:view.toolInfo.toolName];
            
            if (useCount>=freeColorUsage) {
               
//                [Appirater setAppId:APP_ITUNES_ID];
//                
//                [Appirater setDaysUntilPrompt:1];
//                [Appirater setUsesUntilPrompt:0];
//                [Appirater setSignificantEventsUntilPrompt:-1];
//                [Appirater setTimeBeforeReminding:3];
//                [Appirater setDebug:YES];
//                [[Appirater sharedInstance] setDelegate:self];
//                
//                [Appirater appLaunched:YES];
//                
//                if([SKStoreReviewController class]){
//                    [SKStoreReviewController requestReview] ;
//                }
                
              
            }
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:1] forKey:view.toolInfo.toolName];
            
        }

    }
//    else if(view.tag<=3){
//  
//    }
    
    
}

- (UIImage*)filteredImage:(UIImage*)image withToolInfo:(ImageToolInfo*)info
{
    @autoreleasepool {
        Class filterClass = NSClassFromString(info.toolName);
        if([(Class)filterClass conformsToProtocol:@protocol(ColorizeBaseProtocol)]){
            return [filterClass applyFilter:image];
        }
        return nil;
    }
}



@end
