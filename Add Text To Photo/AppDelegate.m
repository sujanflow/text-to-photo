//
//  AppDelegate.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 6/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachability.h"
//#import "Add_Text_To_Photo-Swift.h"
@import StoreKit;

@interface AppDelegate ()<SKRequestDelegate>{
    
    Reachability *networkReachability;
    BOOL isNetworkAvailable;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    if ([self checkForNetworkAvailability]) {
        
        [self checkSubscription];

    }
    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void) checkSubscription{
    
    NSString *pathAndFileName = [[[NSBundle mainBundle] appStoreReceiptURL]path];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathAndFileName])
    {
        NSLog(@"File exists in BUNDLE");
        
        NSData *data = [[NSFileManager defaultManager] contentsAtPath:pathAndFileName];
        
        
        NSString *dataString = data.base64Encoding;
        
        NSLog(@"data....%@",dataString);
        
        NSMutableDictionary * receiptDictionary = [[NSMutableDictionary alloc] init];
        
        [receiptDictionary setObject:dataString forKey:@"receipt-data"];
        [receiptDictionary setObject:@"fbf33de1b63f43119508f3bb0f5e3be9" forKey:@"password"];
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:receiptDictionary options:nil error:nil];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://sandbox.itunes.apple.com/verifyReceipt"]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
           // NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
           
            NSArray *inAppArray = [[responseDic objectForKey:@"receipt"]objectForKey:@"in_app"];
            
            NSMutableArray *monthlySubArray = [[NSMutableArray alloc]init];
            
            for (int i=0; i<inAppArray.count; i++) {
                
                NSDictionary *temp = [inAppArray objectAtIndex:i];
                
                if([[temp objectForKey:@"product_id"] isEqualToString:@"com.sandbox.texttophoto.monthlysubscription"]){
                    
                    [monthlySubArray addObject:temp];
                    
                }
                
            }
            NSLog(@"Request reply: %@", [monthlySubArray lastObject]);
            
            
            
        }] resume];
        
    }
    else
    {
        NSLog(@"File not found");
        
        SKReceiptRefreshRequest *refreshReceiptRequest = [[SKReceiptRefreshRequest alloc] initWithReceiptProperties:@{}];
        refreshReceiptRequest.delegate = self;
        [refreshReceiptRequest start];
    }
}

- (void)requestDidFinish:(SKRequest *)request {
    if([request isKindOfClass:[SKReceiptRefreshRequest class]])
    {
        //SKReceiptRefreshRequest
        NSURL *receiptUrl = [[NSBundle mainBundle] appStoreReceiptURL];
        if ([[NSFileManager defaultManager] fileExistsAtPath:[receiptUrl path]]) {
            NSLog(@"App Receipt exists");
            
        } else {
            NSLog(@"Receipt request done but there is no receipt");
            
            // This can happen if the user cancels the login screen for the store.
            // If we get here it means there is no receipt and an attempt to get it failed because the user cancelled the login.
            //[self trackFailedAttempt];
        }
    }
}
#pragma mark - Network Reachability
- (BOOL)checkForNetworkAvailability{
    networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if ((networkStatus != ReachableViaWiFi) && (networkStatus != ReachableViaWWAN)) {
        isNetworkAvailable = FALSE;
    }else{
        isNetworkAvailable = TRUE;
    }
    
    return isNetworkAvailable;
}


@end
