//
//  ShopTableViewCell.h
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 9/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *offerImageView;
@property (weak, nonatomic) IBOutlet UILabel *offerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *offerDetailLabel;
@property (weak, nonatomic) IBOutlet UIButton *offerPriceButton;



@end

NS_ASSUME_NONNULL_END
