//
//  ShopTableViewCell.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 9/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "ShopTableViewCell.h"
#import "HexColors.h"

@implementation ShopTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.offerPriceButton.layer.borderWidth = 1;
    self.offerPriceButton.layer.borderColor = [[UIColor whiteColor] CGColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
