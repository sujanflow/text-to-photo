//
//  SelectPhotoViewController.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 6/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "SelectPhotoViewController.h"
#import "ImageEditor.h"
#import "ShopViewController.h"

@interface SelectPhotoViewController ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,ImageEditorDelegate, ImageEditorTransitionDelegate, ImageEditorThemeDelegate>{
    
    NSInteger selectedIndex;
}

@end



@implementation SelectPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
     @autoreleasepool {
    // Fetch all assets, sorted by date created.
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    _assetsFetchResults = [PHAsset fetchAssetsWithOptions:options];
    
     }
    
    _imageManager = [[PHImageManager alloc] init];
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//      self.adBannerView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
//      self.adBannerView.rootViewController = self;
//      [self.adBannerView loadRequest:[GADRequest request]];
//        
//    });
}
- (void)viewWillAppear:(BOOL)animated{

    [self.navigationController setNavigationBarHidden:YES];
    
//    PHAsset *asset = _assetsFetchResults[0];
//
//    [_imageManager requestImageForAsset:asset targetSize:self.mainImageView.frame.size contentMode:nil options:nil resultHandler:^(UIImage *result, NSDictionary *info)
//     {
//
//         self.mainImageView.image = result;
//
//     }];
    
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_assetsFetchResults count];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell  = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:203];
    
    @autoreleasepool {
        
    PHAsset *asset = _assetsFetchResults[indexPath.item];
    
    
    [_imageManager requestImageForAsset:asset targetSize:CGSizeMake(200, 200) contentMode:nil options:nil resultHandler:^(UIImage *result, NSDictionary *info)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
            imageView.image = result;
             
         });
         
     }];
    
    
    if (indexPath.item == 0) {
        @autoreleasepool {
            [_imageManager requestImageForAsset:asset targetSize:PHImageManagerMaximumSize contentMode:nil options:nil resultHandler:^(UIImage *result, NSDictionary *info)
             {
        
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     self.mainImageView.image = result;
                     
                 });
        
             }];
        }
    }
        
    }
    
    selectedIndex = 0;
    
    [self setfadeInCell];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView viewWithTag:9999].layer.mask=nil;
    [[collectionView viewWithTag:9999]setUserInteractionEnabled:YES];
    [[collectionView viewWithTag:9999] setTag:0];
    
    @autoreleasepool {
    PHAsset *asset = _assetsFetchResults[indexPath.item];
    
    [_imageManager requestImageForAsset:asset targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeAspectFit options:nil resultHandler:^(UIImage *result, NSDictionary *info)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             
             self.mainImageView.image = result;
             
         });
         
     }];
    }
    selectedIndex = indexPath.item;
    
    [self setfadeInCell];
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float padding = 5;
    
    return CGSizeMake((self.collectionView.frame.size.width - padding)/4, (self.collectionView.frame.size.width - padding)/4);
    
}

-(void)setfadeInCell{
    
    // here selectedIndex is the index path of selected cell.
    NSIndexPath *iPath = [NSIndexPath indexPathForItem:selectedIndex inSection:0];
    //get the cell from index path
    UICollectionViewCell *cell1=(UICollectionViewCell *)[self.collectionView cellForItemAtIndexPath:iPath];
    //scroll the collection view to the selected row
    [self.collectionView scrollToItemAtIndexPath:iPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    // initialize the gradient layer
    CAGradientLayer *gradient = [CAGradientLayer layer];
    //set frame of gradient layer
    gradient.frame = cell1.contentView.bounds;
    //chnage the color of mask here
    gradient.colors = @[(id)[UIColor clearColor].CGColor, (id)[UIColor blackColor].CGColor];
    gradient.locations = @[@0.0, @(1.5)];
    cell1.layer.mask = gradient;
    //optional-assign tag to the cell and set interaction
    [cell1 setTag:9999];
    [cell1 setUserInteractionEnabled:NO];
}


- (IBAction)nextButtonAction:(id)sender {
    
    if(self.mainImageView.image){
        ImageEditor *editor = [[ImageEditor alloc] initWithImage:self.mainImageView.image delegate:self];
         [self presentViewController:editor animated:YES completion:nil];
    }
}


- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)removeAdButtonAction:(id)sender {
    
    ShopViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ShopViewController"];
    vc.isForRemoveAd = YES;
    [self.navigationController pushViewController:vc animated:YES];

}


@end
