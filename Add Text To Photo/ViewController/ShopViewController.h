//
//  ShopViewController.h
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 8/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IAPhelper.h"
#import "MBProgressHUD.h"

NS_ASSUME_NONNULL_BEGIN




@interface ShopViewController : UIViewController<IAPhelperDelegate>

@property BOOL isForRemoveAd;

@property (weak, nonatomic) IBOutlet UITableView *shopTableView;


@property (weak, nonatomic) IBOutlet UIView *buyView;
@property (weak, nonatomic) IBOutlet UIView *subscriptionView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


@property (weak, nonatomic) IBOutlet UIButton *monthButton;
@property (weak, nonatomic) IBOutlet UIButton *yearButton;



// InApp Purchase Properties  =========
@property (strong, nonatomic) IAPhelper *IAPHelper;
@property (strong, nonatomic) SKProduct *summerProduct;
@property (strong, nonatomic) SKProduct *duoToneProduct;
@property (strong, nonatomic) SKProduct *vintageProduct;
@property (strong, nonatomic) SKProduct *mattProduct;
@property (strong, nonatomic) SKProduct *bWProduct;
@property (strong, nonatomic) SKProduct *invertProduct;
@property (strong, nonatomic) SKProduct *birthdayProduct;
@property (strong, nonatomic) SKProduct *foodProduct;
@property (strong, nonatomic) SKProduct *friendshipProduct;
@property (strong, nonatomic) SKProduct *gymProduct;
@property (strong, nonatomic) SKProduct *loveProduct;
@property (strong, nonatomic) SKProduct *summerArtProduct;
@property (strong, nonatomic) SKProduct *travelProduct;

@property (strong, nonatomic) SKProduct *monthlyProduct;

    
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UIButton *subscriptionButton;


@end

NS_ASSUME_NONNULL_END
