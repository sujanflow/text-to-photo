//
//  TutorialViewController.h
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 1/6/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TutorialViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *tutorialCollectionView;


@end

NS_ASSUME_NONNULL_END
