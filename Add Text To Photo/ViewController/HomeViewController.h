//
//  HomeViewController.h
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 6/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet GADBannerView *adBannerView;
@property (weak, nonatomic) IBOutlet UIButton *removeAdButton;

@property (weak, nonatomic) IBOutlet UICollectionView *homeCollectionView;



@end

NS_ASSUME_NONNULL_END
