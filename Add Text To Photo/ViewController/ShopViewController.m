//
//  ShopViewController.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 8/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "ShopViewController.h"
#import "Configs.h"
#import "ShopTableViewCell.h"

@interface ShopViewController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    
    MBProgressHUD *loader;
    NSInteger x;
    NSArray* imageArray;
    NSArray* productImageArray;

    
    NSArray* productNameArray;
}

@end

@implementation ShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.shopTableView.delegate = self;
    self.shopTableView.dataSource = self;
    
    x = 1;
    
    imageArray = [NSArray arrayWithObjects: @"1", @"2",@"3",@"4", @"5",@"6",@"7", @"8",@"9",@"10", @"11",@"13",@"14",@"15",@"16",@"17", nil];
    
    productNameArray = [NSArray arrayWithObjects: @"Summer Filter", @"DuoTone Filter",@"Vintage Filter",@"Matte Filter",@"B&W Filter",@"Invert Filter",
                        
        @"Birthday Artwork",@"Food Artwork",@"Friendship Artwork",@"Gym Artwork",@"Love Artwork",@"Summer Artwork",@"Travel Artwork",@"",nil];
    productImageArray = [NSArray arrayWithObjects: @"Group 3122", @"Group 3123",@"Group 3124",@"Group 3125",@"Group 3126",@"Group 3127",
                         
        @"Group 3130",@"Group 3131",@"Group 3132",@"Group 3134",@"Group 3135",@"Group 3136",@"Group 3137",@"",nil];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [NSTimer scheduledTimerWithTimeInterval:2.0
                                     target:self
                                   selector:@selector(autoScroll)
                                   userInfo:nil
                                    repeats:YES];
    
    
//    // ADD PROGRESS HUD ================
    loader = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:loader];
    [loader show:true];
    
    
    // IAPhelper init =============================
    _IAPHelper = [[IAPhelper alloc] init];
    _IAPHelper.delegate = self;

    // IMPORTANT: here you must place the same Product ID
    // you've created into your IAP settings in iTunes
    [_IAPHelper fetchProducts:@[IAP_SUMMERPACK,IAP_DUO,IAP_BW,IAP_INVERT,IAP_MAAT,IAP_VINTAGE,IAP_BIRTHDAY,IAP_FOOD,IAP_FRIEND,IAP_GYM,IAP_LOVE,IAP_SUMMER_ART,IAP_TRAVEL,IAP_MONTHLY,IAP_YEARLY,IAP_ALL,IAP_REMOVE_AD]];
    //============================================================
    
    
    self.monthButton.layer.borderWidth = 1;
    self.monthButton.layer.borderColor = [[UIColor whiteColor] CGColor];

    self.yearButton.layer.borderWidth = 1;
    self.yearButton.layer.borderColor = [[UIColor whiteColor] CGColor];

    NSLog(@"isForRemoveAd %d",self.isForRemoveAd);
    
    if (self.isForRemoveAd) {
    
        _buyButton.selected = YES;
        _buyButton.backgroundColor = [UIColor lightGrayColor];
        [_buyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        _subscriptionButton.selected = NO;
        _subscriptionButton.backgroundColor = [UIColor blackColor];
        [_subscriptionButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        _subscriptionView.hidden = YES;
        _buyView.hidden = NO;
        
    }else{
        _subscriptionButton.selected = YES;
        _subscriptionButton.backgroundColor = [UIColor lightGrayColor];
        [_subscriptionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        _buyButton.selected = NO;
        _buyButton.backgroundColor = [UIColor blackColor];
        [_buyButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        _subscriptionView.hidden = NO;
        _buyView.hidden = YES;

    }
    
    
    
    
}

//-----Collectionview

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell* cell;
    cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"subscriptionCell" forIndexPath:indexPath];
    
    UIImageView * cellImage = [(UIImageView*)cell viewWithTag:2];
    
    cellImage.image = [UIImage imageNamed:[imageArray objectAtIndex:indexPath.item]];
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(80, (CGRectGetHeight(collectionView.frame)));
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return productNameArray.count;
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
   
    
    if (indexPath.section == 0) {
        
        static NSString *CellIdentifier = @"1stCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
        }
        
        return  cell;
    }else{
        
        static NSString *CellIdentifier = @"shopCell";
        ShopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
        {
            cell = [[ShopTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
        }
        
        cell.offerTitleLabel.text = [NSString stringWithFormat:@"%@",[productNameArray objectAtIndex:indexPath.section-1]];
        NSString *imageName = [NSString stringWithFormat:@"%@",[productImageArray objectAtIndex:indexPath.section-1]];
        cell.offerImageView.image = [UIImage imageNamed:imageName];
        
        
        return cell;
    }
    
    
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        
        
    }else if (indexPath.section == 1){
        
        [_IAPHelper purchaseItem:_summerProduct];
        
        
        [loader show:true];
    }else if (indexPath.section == 2){
        
        [_IAPHelper purchaseItem:_duoToneProduct];
        
        
        [loader show:true];
    }else if (indexPath.section == 3){
        
        [_IAPHelper purchaseItem:_vintageProduct];
        
        
        [loader show:true];
    }else if (indexPath.section == 4){
        
        [_IAPHelper purchaseItem:_mattProduct];
        
        
        [loader show:true];
    }else if (indexPath.section == 5){
        
        [_IAPHelper purchaseItem:_bWProduct];
        
        
        [loader show:true];
    }else if (indexPath.section == 6){
        
        [_IAPHelper purchaseItem:_invertProduct];
        
        
        [loader show:true];
    }else if (indexPath.section == 7){
        
        [_IAPHelper purchaseItem:_birthdayProduct];
        
        
        [loader show:true];
    }else if (indexPath.section == 8){
        
        [_IAPHelper purchaseItem:_foodProduct];
        
        
        [loader show:true];
    }else if (indexPath.section == 9){
        
        [_IAPHelper purchaseItem:_friendshipProduct];
        
        
        [loader show:true];
    }else if (indexPath.section == 10){
        
        [_IAPHelper purchaseItem:_gymProduct];
        
        
        [loader show:true];
        
    }else if (indexPath.section == 11){
        
        [_IAPHelper purchaseItem:_loveProduct];
        
        
        [loader show:true];
    }else if (indexPath.section == 12){
        
        [_IAPHelper purchaseItem:_summerArtProduct];
        
        
        [loader show:true];
    }else if (indexPath.section == 13){
        
        [_IAPHelper purchaseItem:_travelProduct];
        
        
        [loader show:true];
    }
//    else if (indexPath.section == 14){
//
//
//    }else if (indexPath.section == 15){
//
//        [_IAPHelper purchaseItem:_summerProduct];
//
//
//        [loader show:true];
//    }else if (indexPath.section == 16){
//
//        [_IAPHelper purchaseItem:_summerProduct];
//
//
//        [loader show:true];
//    }else if (indexPath.section == 17){
//
//        [_IAPHelper purchaseItem:_summerProduct];
//
//
//        [loader show:true];
//    }else if (indexPath.section == 18){
//
//        [_IAPHelper purchaseItem:_summerProduct];
//
//
//        [loader show:true];
//    }
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        return 230;
    }
    
    return 120;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 5)];
    view.backgroundColor = [UIColor clearColor];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 5;
}

- (IBAction)buyButtonAction:(UIButton*)sender {
    
    if (!_buyButton.selected) {
        
        _buyButton.selected = YES;
        _buyButton.backgroundColor = [UIColor lightGrayColor];
        [_buyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        _subscriptionButton.selected = NO;
        _subscriptionButton.backgroundColor = [UIColor blackColor];
        [_subscriptionButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        _subscriptionView.hidden = YES;
        _buyView.hidden = NO;
        
    }
    
}

- (IBAction)subscriptionButtonAction:(UIButton*)sender {
    
    if (!_subscriptionButton.selected) {
        
        _subscriptionButton.selected = YES;
        _subscriptionButton.backgroundColor = [UIColor lightGrayColor];
        [_subscriptionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        _buyButton.selected = NO;
        _buyButton.backgroundColor = [UIColor blackColor];
        [_buyButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        _subscriptionView.hidden = NO;
        _buyView.hidden = YES;
        
    }

}

-(void)autoScroll{
    
    if (x<imageArray.count) {
        
        NSIndexPath *index = [NSIndexPath indexPathForItem:x inSection:0];
        
        [self.collectionView scrollToItemAtIndexPath:index atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        
        x = x + 1;
        
    }else{
        
        x = 0;
        
        [self.collectionView scrollToItemAtIndexPath:0 atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
    
}
- (IBAction)monthlyButtonAction:(id)sender {
    
    if (!_monthButton.selected) {
        
        _monthButton.selected = YES;
        _monthButton.backgroundColor = [UIColor whiteColor];
        
        _yearButton.selected = NO;
        _yearButton.backgroundColor = [UIColor clearColor];
        
        
    }

}
- (IBAction)yearlyButtonAction:(id)sender {
    
    if (!_yearButton.selected) {
        
        _yearButton.selected = YES;
        _yearButton.backgroundColor = [UIColor whiteColor];
        
        _monthButton.selected = NO;
        _monthButton.backgroundColor = [UIColor clearColor];
        
    }
}


- (IBAction)subscribeButtonAction:(id)sender {
    
    if (_yearButton.selected) {
        
//        [_IAPHelper purchaseItem:_summerProduct];
//
//
//        [loader show:true];
        
    }else if (_monthButton.selected){
        
        [_IAPHelper purchaseItem:_monthlyProduct];
        
        
        [loader show:true];
    }
    
}

- (IBAction)turmsButtonAction:(id)sender {
    
}

- (IBAction)privacyButtonAction:(id)sender {
    
}


#pragma mark - IAP DELEGATE METHODS ==============================


- (void)bankerFailedRestorePurchases {
    

    
}

// Purchase restored
- (void)bankerDidRestorePurchases {
    
}

- (void)bankerFailedToConnect {
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle: APP_NAME
                                                 message:NSLocalizedString(@"iTunes Store connection failed, try again!", @"") delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                       otherButtonTitles:nil];
    [av show];
    
    [loader hide:true];


}

- (void)bankerFoundInvalidProducts:(NSArray *)products {
    
    NSLog(@"bankerFoundInvalidProducts");
    [loader hide:true];

}

- (void)bankerFoundProducts:(NSArray *)products {
    
    NSLog(@"bankerFoundProducts %@",products);
    
 
    
    for (SKProduct *product in products) {
        
        if ([product.productIdentifier isEqualToString:IAP_SUMMERPACK]){
            
            self.summerProduct = product;
            
        }else  if ([product.productIdentifier isEqualToString:IAP_MAAT]) {
            
            self.mattProduct = product;
            
        }else  if ([product.productIdentifier isEqualToString:IAP_BW]) {
            
            self.bWProduct = product;
            
        }else  if ([product.productIdentifier isEqualToString:IAP_INVERT]) {
            
            self.invertProduct = product;
            
        }else  if ([product.productIdentifier isEqualToString:IAP_DUO]) {
            
            self.duoToneProduct = product;
            
        }else  if ([product.productIdentifier isEqualToString:IAP_VINTAGE]) {
            
            self.vintageProduct = product;
        }
        
        else  if ([product.productIdentifier isEqualToString:IAP_BIRTHDAY]) {
            
            self.birthdayProduct = product;
        }else  if ([product.productIdentifier isEqualToString:IAP_FOOD]) {
            
            self.foodProduct = product;
        }else  if ([product.productIdentifier isEqualToString:IAP_GYM]) {
            
            self.gymProduct = product;
        }else  if ([product.productIdentifier isEqualToString:IAP_FRIEND]) {
            
            self.friendshipProduct = product;
        }else  if ([product.productIdentifier isEqualToString:IAP_LOVE]) {
            
           self.loveProduct = product;
        }else  if ([product.productIdentifier isEqualToString:IAP_SUMMER_ART]) {
            
           self.summerArtProduct = product;
        }else  if ([product.productIdentifier isEqualToString:IAP_TRAVEL]) {
            
            self.travelProduct = product;
        }
        
        else  if ([product.productIdentifier isEqualToString:IAP_MONTHLY]) {
            
            self.monthlyProduct = product;
        }
        else  if ([product.productIdentifier isEqualToString:IAP_YEARLY]) {
            
           
        }
        
        else  if ([product.productIdentifier isEqualToString:IAP_ALL]) {
            
            
        }
        else  if ([product.productIdentifier isEqualToString:IAP_REMOVE_AD]) {
            
            
        }

        NSLog(@"_product.productIdentifier %@",product.productIdentifier);
        
    }
    
    [loader hide:true];

}

- (void)bankerNoProductsFound {
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle: APP_NAME
                                                 message:NSLocalizedString(@"bankerNoProductsFound, try again!", @"") delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                       otherButtonTitles:nil];
    [av show];
    
    [loader hide:true];

}

// IAP purchase successfully made...
- (void)bankerProvideContent:(SKPaymentTransaction *)paymentTransaction {
    
    NSLog(@"bankerProvideContent....IAP purchase successfully made");
    
    
    [loader hide:true];

}

- (void)bankerPurchaseCancelledByUser:(NSString *)productIdentifier {
    
    NSLog(@"bankerPurchaseCancelledByUser");
    [loader hide:true];

}
// IAP Payment complete...
- (void)bankerPurchaseComplete:(SKPaymentTransaction *)paymentTransaction {
    
    NSLog(@"bankerPurchaseComplete %@",paymentTransaction.payment.productIdentifier);
    
    if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_SUMMERPACK]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeSummerPack"];

    }else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_MAAT]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeMatt"];
        
    }else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_BW]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeB&W"];
        
    }else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_INVERT]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeInvert"];
        
    }else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_DUO]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeDuo"];
        
    }else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_VINTAGE]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeVintage"];
    }
    
    
    else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_BIRTHDAY]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeBirthDay"];
    }else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_FOOD]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeFood"];
    }else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_GYM]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeGym"];
    }else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_FRIEND]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeFriend"];
    }else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_LOVE]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeLove"];
    }else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_SUMMER_ART]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeSummerArt"];
    }else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_TRAVEL]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeTravel"];
    }
    
    
    else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_MONTHLY]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeMonthly"];
    }
    else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_YEARLY]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeYearly"];
    }
    
    
    else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_ALL]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeAll"];
    }
    else  if ([paymentTransaction.payment.productIdentifier isEqualToString:IAP_REMOVE_AD]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIapMadeRemoveAD"];
    }
    
    [loader hide:true];

}

- (void)bankerPurchaseFailed:(NSString *)productIdentifier withError:(NSString *)errorDescription {
    NSLog(@"bankerPurchaseFailed");
    [loader hide:true];

}


@end
