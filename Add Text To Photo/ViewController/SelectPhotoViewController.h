//
//  SelectPhotoViewController.h
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 6/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelectPhotoViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property(nonatomic , strong) PHFetchResult *assetsFetchResults;
@property(nonatomic , strong) PHImageManager *imageManager;

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;


@property (weak, nonatomic) IBOutlet GADBannerView *adBannerView;
@property (weak, nonatomic) IBOutlet UIButton *removeAdButton;


@end

NS_ASSUME_NONNULL_END
