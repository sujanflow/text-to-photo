//
//  HomeViewController.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 6/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "HomeViewController.h"
#import "KYDrawerController.h"
#import <Photos/Photos.h>
#import "SelectPhotoViewController.h"
#import "ShopViewController.h"
#import "ImageEditor.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface HomeViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,ImageEditorDelegate, ImageEditorTransitionDelegate, ImageEditorThemeDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    
    NSInteger x;
    NSArray* imageArray;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    x = 1;
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//       
//   
//        self.adBannerView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
//        self.adBannerView.rootViewController = self;
//        [self.adBannerView loadRequest:[GADRequest request]];
//
//     });
    
    imageArray = [NSArray arrayWithObjects: @"launch_back", @"landing_back", nil];
    
    self.homeCollectionView.delegate = self;
    self.homeCollectionView.dataSource = self;
    
    [NSTimer scheduledTimerWithTimeInterval:2.0
                                     target:self
                                   selector:@selector(autoScroll)
                                   userInfo:nil
                                    repeats:YES];
    
}

- (void)viewWillAppear:(BOOL)animated{

    [self.navigationController setNavigationBarHidden:YES];
    
}
- (IBAction)cameraButtonAction:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)libraryButtonAction:(id)sender {
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus authorizationStatus) {
        if (authorizationStatus == PHAuthorizationStatusAuthorized) {
            
            NSLog(@"PHAuthorizationStatusAuthorized");
            dispatch_async(dispatch_get_main_queue(), ^{
                
                SelectPhotoViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectPhotoViewController"];
                
                [self.navigationController pushViewController:vc animated:YES];
                
            });
            
        }else if (authorizationStatus == PHAuthorizationStatusDenied){
            
            NSLog(@"PHAuthorizationStatusDenied");
        }
    }];
}

//Image picker delegate------
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    //self.imageView.image = chosenImage;
    
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        if(chosenImage){
            ImageEditor *editor = [[ImageEditor alloc] initWithImage:chosenImage delegate:self];
            [self presentViewController:editor animated:YES completion:nil];
        }
        
    }];
    
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
//------------


//-----Collectionview

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell* cell;
    cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"homeMenuCell" forIndexPath:indexPath];
    
    UIImageView * cellImage = [(UIImageView*)cell viewWithTag:1];
    
    cellImage.image = [UIImage imageNamed:[imageArray objectAtIndex:indexPath.item]];
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return self.homeCollectionView.frame.size;
}

-(void)autoScroll{
    
    if (x<imageArray.count) {
        
        NSIndexPath *index = [NSIndexPath indexPathForItem:x inSection:0];
        
        [self.homeCollectionView scrollToItemAtIndexPath:index atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        
        x = x + 1;
    
    }else{
        
        x = 0;
        
        [self.homeCollectionView scrollToItemAtIndexPath:0 atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
    
}

//@objc func autoScroll() {
//    if self.x < 4 {
//        let indexPath = IndexPath(item: x, section: 0)
//        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//        self.x = self.x + 1
//    } else {
//        self.x = 0
//        self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
//    }
//}



- (IBAction)shopButtonAction:(id)sender {
    
    ShopViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ShopViewController"];
    
    [self.navigationController pushViewController:vc animated:YES];

    
}


- (IBAction)menuButtonAction:(id)sender {
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
}

- (IBAction)removeAdbuttonAction:(id)sender {
    
    ShopViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ShopViewController"];
    vc.isForRemoveAd = YES;
    [self.navigationController pushViewController:vc animated:YES];

}



@end
