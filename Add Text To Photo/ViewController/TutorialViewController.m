//
//  TutorialViewController.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 1/6/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    
    NSInteger x;
    NSArray* imageArray;
}


@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    x = 1;
    
    imageArray = [NSArray arrayWithObjects: @"launch_back", @"landing_back", nil];
    
    self.tutorialCollectionView.delegate = self;
    self.tutorialCollectionView.dataSource = self;
}

//-----Collectionview

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell* cell;
    cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"tutorialCell" forIndexPath:indexPath];
    
    UIImageView * cellImage = [(UIImageView*)cell viewWithTag:10];
    
    cellImage.image = [UIImage imageNamed:[imageArray objectAtIndex:indexPath.item]];
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return self.tutorialCollectionView.frame.size;
}


- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
