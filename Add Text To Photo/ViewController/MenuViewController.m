//
//  MenuViewController.m
//  Add Text To Photo
//
//  Created by Md.Ballal Hossen on 6/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "MenuViewController.h"
#import "KYDrawerController.h"
#import "SelectPhotoViewController.h"
#import "HomeViewController.h"
#import "ShopViewController.h"
#import "TutorialViewController.h"

@interface MenuViewController (){
    
    NSArray* menuArray;
    NSArray* menuImageArray;
}

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    menuArray = [NSArray arrayWithObjects: @"Tutorial", @"Shopping", @"Rate us", @"Feedback", nil];
    menuImageArray = [NSArray arrayWithObjects: @"tutorial_icon", @"shopping_icon", @"rateUs_icon", @"feedback_icon", nil];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"menuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
   
    UIImageView *menuIcon = (UIImageView*)[cell viewWithTag:201];
    menuIcon.image = [UIImage imageNamed:[menuImageArray objectAtIndex:indexPath.row]];
    
    UILabel *menuTitle = (UILabel*)[cell viewWithTag:202];
    menuTitle.text = [menuArray objectAtIndex:indexPath.row];
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath{
    [tableView deselectRowAtIndexPath:newIndexPath animated:YES];
    
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    HomeViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:viewController];
    
    switch ([newIndexPath row]) {
        case 0:{
            
            TutorialViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TutorialViewController"];
            
            [navController pushViewController:vc animated:YES];
   
            break;
        }
            
        case 1:{
            
            ShopViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ShopViewController"];
            
            [navController pushViewController:vc animated:YES];
            break;
        }
        case 2:{
            //https://itunes.apple.com/us/app/add-text-to-photos-free-letter-fonts-for-image-put/id1067649045?mt=8
            NSString * theUrl = [NSString  stringWithFormat:@"https://itunes.apple.com/us/app/add-text-to-photos-free-letter-fonts-for-image-put/id1067649045?mt=8"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
            
            break;
        }
        case 3:{
            
            /* create mail subject */
            NSString *subject = [NSString stringWithFormat:@"Feedback"];
            
            /* define email address */
            NSString *mail = [NSString stringWithFormat:@"test@test.com"];
            
            /* define allowed character set */
            NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
            
            /* create the URL */
            NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"mailto:?to=%@&subject=%@",
                                                        [mail stringByAddingPercentEncodingWithAllowedCharacters:set],
                                                        [subject stringByAddingPercentEncodingWithAllowedCharacters:set]]];
            /* load the URL */
            [[UIApplication sharedApplication] openURL:url];
            
            break;
            
        }

            
        default:{
            [viewController.view setBackgroundColor:[UIColor whiteColor]];
            break;
        }
            
            
            
            
    }
    elDrawer.mainViewController=navController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:YES];
}

@end
