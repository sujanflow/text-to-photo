//
//  Configs.h
#ifndef Add_Text_Configs_h
#define Add_Text_Configs_h




// APP INFO ================================
#define APP_NAME @""

#define APP_ITUNES_ID @""

// Replace "XXXXXXX" with your own App ID, you can find it by clicking on "More -> About This App" button in iTunes Connect, copy the Apple ID and paste it here
#define RATE_US_LINK @""

// Replace the link below with the one of your app
#define ITUNES_STORE_LINK @""

#define FEEDBACK_EMAIL_ADDRESS @""
//-----------------------------------------------------------------------------


// Google admob App id

#define ADMOB_APP_ID @""

#define ADMOB_APP_ID_FOR_NATIVE @""

#define ADMOB_BANNER_ID @""
#define ADMOB_NATIVE_ID @""
#define ADMOB_INTERSTITIAl_ID @""


// COLORS ===============================
#define MAIN_COLOR [UIColor colorWithRed:47.0/255.0 green:55.0/255.0 blue:65.0/255.0 alpha:1.0]
#define LIGHT_COLOR [UIColor colorWithRed:246.0/255.0 green:247.0/255.0 blue:251.0/255.0 alpha:1.0]
#define PURPLE_COLOR [UIColor colorWithRed:209.0/255.0 green:118.0/255.0 blue:220.0/255.0 alpha:1.0]
#define MENU_COLOR [UIColor colorWithRed:209.0/255.0 green:211.0/255.0 blue:212.0/255.0 alpha:1.0]

//-----------------------------------------------------------------------------


// FONTS ===============================
#define MAIN_FONT [UIFont fontWithName:@"HelveticaNeue-Thin" size:10]
#define REGULAR_FONT [UIFont fontWithName:@"HelveticaNeue" size:13]

#define NAVBAR_FONT [UIFont fontWithName:@"BebasNeue" size:15]
//-----------------------------------------------------------------------------


// YOUR INSTAGRAM PAGE URL =====================================
#define INSTAGRAM_URL @""
//-----------------------------------------------------------------------------


// YOUR INSTAGRAM PAGE URL =====================================
#define FACEBOOK_PAGE_LINK @""
//-----------------------------------------------------------------------------


// SHARING MESSAGE FOR TWITTER, FACEBOOK AND MAIL ===================
#define SHARING_TITLE @"Hi there!"
#define SHARING_MESSAGE @""
#define FACEBOOK_LOGIN_ALERT @"Please go to Settings and link your Facebook account to this device!"
#define TWITTER_LOGIN_ALERT @"Please go to Settings and link your Twitter account to this device!"
//-----------------------------------------------------------------------------


// IN-APP PURCHASE PRODUCT ID ========================
#define IAP_SUMMERPACK @"com.sandbox.texttophoto.filtersummer"
#define IAP_REMOVE_AD @"com.sandbox.texttophoto.removead"
//
#define IAP_DUO @"com.sandbox.texttophoto.filterduotone"
#define IAP_VINTAGE @"com.sandbox.texttophoto.filtervintage"
#define IAP_MAAT @"com.sandbox.texttophoto.filtermatte"
#define IAP_BW @"com.sandbox.texttophoto.filterbandw"
#define IAP_INVERT @"com.sandbox.texttophoto.filterinvert"
#define IAP_BIRTHDAY @"com.sandbox.texttophoto.artworkbirthday"
#define IAP_FOOD @"com.sandbox.texttophoto.artworkfood"
#define IAP_FRIEND @"com.sandbox.texttophoto.artworkfriendship"
#define IAP_GYM @"com.sandbox.texttophoto.artworkgym"
#define IAP_LOVE @"com.sandbox.texttophoto.artworklove"
#define IAP_SUMMER_ART @"com.sandbox.texttophoto.artworksummer"
#define IAP_TRAVEL @"com.sandbox.texttophoto.artworktravel"

#define IAP_ALL @"com.sandbox.texttophoto.unlockall"


#define IAP_MONTHLY @"com.sandbox.texttophoto.monthlysubscription"
#define IAP_YEARLY @"com.sandbox.texttophoto.yearlysubscription"
//-----------------------------------------------------------------------------

#define shouldShowFBAdd NO
#define freeDaysForRating 3

// FREE ITEMS AVAILABLE WITH NO IAP ================
#define freeStickers 100     +1
#define freeFrames   9      +1
#define freeFonts   9      +1
#define freeFilters   2      +1
#define freeEffects   9      +1
#define freeBorders  9      +1
#define freeTextures 9      +1
//-----------------------------------------------------------------------------

// USE COUNTS BEFORE RATING ================
#define freeColorUsage 3
#define freeEffectUsage  3
#define freeFilterUsage  3
#define freeTextUsage 3
//-----------------------------------------------------------------------------



#endif
